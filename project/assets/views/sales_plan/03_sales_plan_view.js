
$(document).ready(function(){   
    
    $('#datepicker').datepicker({
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months"
    });
    
});


var tableToExcel = (function () {
     var uri = 'data:application/vnd.ms-excel;base64,'
   , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head>\
<!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head>\
<body><table>{table}</table></body></html>'
   , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
   , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
       return function (table, name) {
           if (!table.nodeType) table = document.getElementById(table)
           var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
           window.location.href = uri + base64(format(template, ctx))
       }
})();


$('#03-btn-export').click(function(){
    tableToExcel('03-sales-plan-table', 'Sales Plan');
});




$('#03-btn-search').click(function(){
    
    var fdate = $('#03-from-date').val();
    var tdate = $('#03-to-date').val();
    if(fdate == '' || fdate == null || tdate == '' || tdate == null)
        return;
    var fd = new Date(fdate+'-01');
    var td = new Date(tdate+'-01');
    if(fd.getTime() > td.getTime())
        return;
    
    waitingDialog.show('Loading', {dialogSize: 'sm', progressType: 'primary'});  
    
    var td_blue = {
        back : '#3c8dbc',
        text : '#ffffff'
    }
    var td_green = '#9ec9e2';//#5cb85c';
    var td_target = '#dbedff';//'#ebebe0';
    var td_target_title = '#d3dde8';// '#e1e1d1';
    var td_confirmed = '#d4e8fc';//'#ffd9b3';
    var td_confirmed_title = '#c7d9ea';// '#ffcc99';
    var td_projection = '#c9e3fc';// '#d6f5d6';
    var td_projection_title = '#bbd2e8';// '#c2f0c2';
    var td_previous = '#bfdffc';// '#ffd6cc';
    var td_previous_title = '#aecae5';// '#ffc2b3';
       
   /* var fdate = $('#03-from-date').val();
    var tdate = $('#03-to-date').val();*/
    
    var qty_confirmed_sub_totals = [];
    var qty_target_sub_totals = [];
    var qty_projection_sub_totals = [];
    var qty_previous_sub_totals = [];
    
    make_ajax_request({
        url : BASE_URL+'index.php/sales_plan/get_sales_plan',
        async : true,
        data : {'from_date' : fdate , 'to_date' : tdate},
        success : function(response){
            
    var obj = JSON.parse(response);
    var len = obj.length;
    var month_count = obj[0]['details'].length;
    
    var thead = $('#03-sales-plan-table thead');
    thead.empty();
    var thead_str = '<tr> <th class="headcol" style="min-width:140px;background:'+td_blue['back']+';color:'+td_blue['text']+'">Month</th>';
    for(y=0 ; y < month_count ; y++)
    {        
        thead_str += '<th style="text-align: center;background:'+td_blue['back']+';color:'+td_blue['text']+'" colspan="4">'+obj[0]['details'][y]['month']+'</th>';       
    }
    thead_str += '</tr>';
    thead.append(thead_str);
    
    var tbl = $('#03-sales-plan-table tbody');
    tbl.empty();
    var tbl_header = $('<tr></tr>');
    tbl_header.append('<td class="headcol" style="background:'+td_green+';margin-left:15px">Buyer</td>');
    tbl.append(tbl_header);
    var th_str = '';
    
    /*$('#03-plan-table-first thead').html('<tr><th style="min-width:120px;">Month</th></tr><tr><th>Buyer</th></tr>');
    var ttt = '';
     for(x = 0 ; x < len ; x++)
    {
        ttt += '<tr> <td style="background:'+td_green+';margin-left:15px">'+obj[x]['buyer']+'</td>';
    }
    $('#03-plan-table-first tbody').html(ttt);*/
    
    for(y=0 ; y < month_count ; y++)
    {
        th_str += '<td class="title" style="background:'+td_target_title+'">Target</td>\
        <td class="title" style="background:'+td_confirmed_title+'">Confirmed</td>\
        <td class="title" style="background:'+td_projection_title+'">Projection</td>\
        <td class="title" style="background:'+td_previous_title+';min-width:100px">Prev. Year</td>';
    }
    tbl_header.append(th_str);
    var td_str = '';
    for(x = 0 ; x < len ; x++)
    {
        td_str += '<tr> <td class="headcol" style="background:'+td_green+';margin-left:15px">'+obj[x]['buyer_description']+'</td>';
        
        for(y=0 ; y < month_count ; y++)
        {
            //var qty = obj[x]['details'][y]['qty_confirmed'];
            var month = obj[x]['details'][y]['month'];
            //var input_attr = 'type="text" class="form-control input-sm currency" data-buyer="'+obj[x]['buyer']+'" data-month="'+month+'"';
            
            var qty_confirmed = obj[x]['details'][y]['qty_confirmed'];
            var qty_target = obj[x]['details'][y]['qty_target'];
            var qty_projection = obj[x]['details'][y]['qty_projection'];
            var qty_previous = obj[x]['details'][y]['qty_previous'];
            
            if(qty_target != null && qty_target != '')
            {
                qty_target = parseFloat(qty_target);
                td_str += '<td class="currency" style="background:'+td_target+'">'+qty_target.toLocaleString()+'</td>';
            }
            else
            {
                qty_target = 0;
                td_str += '<td style="background:'+td_target+'"></td>';
            }
            
            
            if(/*obj[x]['details'][y]['qty_confirmed'] */ qty_confirmed != null && qty_confirmed != '')
            {
                qty_confirmed = parseFloat(qty_confirmed);
                td_str += '<td class="currency" style="background:'+td_confirmed+'">'+qty_confirmed.toLocaleString()+'</td>';
            }                
            else
            {
                qty_confirmed = 0;
                td_str += '<td style="background:'+td_confirmed+'"> </td>';
            }
                
            
            if(qty_projection != null && qty_projection != '')
            {
                qty_projection = parseFloat(qty_projection);
                td_str += '<td class="currency" style="background:'+td_projection+'">'+qty_projection.toLocaleString()+'</td>';
            }
            else
            {
                qty_projection = 0;
                 td_str += '<td style="background:'+td_projection+'"></td>';
            }
            
            if(qty_previous != null && qty_previous != '')
            {
                qty_previous = parseFloat(qty_previous);
                td_str += '<td class="currency" style="background:'+td_previous+'">'+qty_previous.toLocaleString()+'</td>';            
            }
            else
            {
                qty_previous = 0;
                td_str += '<td style="background:'+td_previous+'"></td>';
            }
           
            
            qty_confirmed_sub_totals[y] == undefined ? qty_confirmed_sub_totals[y] = qty_confirmed : qty_confirmed_sub_totals[y] += qty_confirmed;
            qty_previous_sub_totals[y] == undefined ? qty_previous_sub_totals[y] = qty_previous : qty_previous_sub_totals[y] += qty_previous;
            qty_projection_sub_totals[y] == undefined ? qty_projection_sub_totals[y] = qty_projection : qty_projection_sub_totals[y] += qty_projection;
            qty_target_sub_totals[y] == undefined ? qty_target_sub_totals[y] = qty_target : qty_target_sub_totals[y] += qty_target;
            
        }
        
        td_str += '</tr>';
    }
    
    td_str += '<tr><td class="headcol" style="background:'+td_blue['back']+';color:'+td_blue['text']+';margin-left:15px">Sub Total</td>';
    for(y=0 ; y < month_count ; y++)
    {        
        td_str += '<td class="currency" style="background:'+td_target+'">'+qty_target_sub_totals[y].toLocaleString()+'</td>\
            <td  class="currency" style="background:'+td_confirmed+'">'+qty_confirmed_sub_totals[y].toLocaleString()+'</td>\
            <td class="currency" style="background:'+td_projection+'">'+qty_projection_sub_totals[y].toLocaleString()+'</td> \
            <td class="currency" style="background:'+td_previous+'">'+qty_previous_sub_totals[y].toLocaleString()+'</td>';       
    }
    td_str += '</tr>';
    
    td_str += '<tr><td class="headcol" style="background:'+td_blue['back']+';color:'+td_blue['text']+';margin-left:15px">Total Confirmed</td>';
    for(y=0 ; y < month_count ; y++)
    {        
        td_str += '<td class="currency" style="background:'+td_confirmed+';text-align:center" colspan="4">'+qty_confirmed_sub_totals[y].toLocaleString()+'</td>';       
    }
    td_str += '</tr>';
    
    td_str += '<tr><td class="headcol" style="background:'+td_blue['back']+';color:'+td_blue['text']+';margin-left:15px;">Total Target</td>';
    for(y=0 ; y < month_count ; y++)
    {        
        td_str += '<td class="currency" style="background:'+td_target+';text-align:center" colspan="4">'+qty_target_sub_totals[y].toLocaleString()+'</td>';       
    }
    td_str += '</tr>';
    
    tbl.append(td_str);
    $('#03-sales-plan-table').append(tbl);
    
    waitingDialog.hide();

        }
    });

});




