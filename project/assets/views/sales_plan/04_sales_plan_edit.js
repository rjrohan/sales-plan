
$(document).ready(function(){   
    
    $('#datepicker').datepicker({
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months"
    });
    
});



$('#04-btn-search').click(function(){
    
    var fdate = $('#04-from-date').val();
    var tdate = $('#04-to-date').val();
    
    if(fdate == '' || fdate == null || tdate == '' || tdate == null)
        return;
    var fd = new Date(fdate+'-01');
    var td = new Date(tdate+'-01');
    if(fd.getTime() > td.getTime())
        return;
    
    waitingDialog.show('Processing...', {dialogSize: 'sm', progressType: 'primary'});     
       
    /*var fdate = $('#04-from-date').val();
    var tdate = $('#04-to-date').val();*/
    
    var qty_confirmed_sub_totals = [];
    var qty_target_sub_totals = [];
    var qty_projection_sub_totals = [];
    var qty_previous_sub_totals = [];
    
    make_ajax_request({
        url : BASE_URL+'index.php/sales_plan/get_sales_plan',
        async : true,
        data : {'from_date' : fdate , 'to_date' : tdate},
        success : function(response){
            
    var obj = JSON.parse(response);
    var len = obj.length;
    var month_count = obj[0]['details'].length;
    
    var thead = $('#04-sales-plan-table thead');
    thead.empty();
    var thead_str = '<tr style="background: #B5D1D8"> <th>Month</th>';
    for(y=0 ; y < month_count ; y++)
    {        
        thead_str += '<th style="text-align: center;" colspan="4">'+obj[0]['details'][y]['month']+'</th>';       
    }
    thead_str += '</tr>';
    thead.append(thead_str);
    
    var tbl = $('#04-sales-plan-table tbody');
    tbl.empty();
    var tbl_header = $('<tr></tr>');
    tbl_header.append('<td style="min-width: 120px;">Buyer</td>');
    tbl.append(tbl_header);
    var th_str = '';
    
    for(y=0 ; y < month_count ; y++)
    {
        th_str += '<td class="title" style="min-width: 120px;">Target</td>\
        <td class="title" style="min-width: 120px;">Confirmed</td>\
        <td class="title" style="min-width: 120px;">Projection</td>\
        <td class="title" style="min-width: 120px;">Previous</td>';
    }
    tbl_header.append(th_str);
    var td_str = '';
    for(x = 0 ; x < len ; x++)
    {
        td_str += '<tr> <td>'+obj[x]['buyer_description']+'</td>';
        
        for(y=0 ; y < month_count ; y++)
        {
            //var qty = obj[x]['details'][y]['qty_confirmed'];
            var month = obj[x]['details'][y]['month'];
            var input_attr = 'type="text" class="form-control input-sm currency" data-buyer="'+obj[x]['buyer']+'" data-month="'+month+'"';
            
            var qty_confirmed = obj[x]['details'][y]['qty_confirmed'];
            var qty_target = obj[x]['details'][y]['qty_target'];
            var qty_projection = obj[x]['details'][y]['qty_projection'];
            var qty_previous = obj[x]['details'][y]['qty_previous'];
            
            if(qty_target != null && qty_target != '')
            {
                qty_target = parseFloat(qty_target);
                td_str += '<td>\
                    <input '+input_attr+' data-type="target" value="'+qty_target.toLocaleString()+'">\
                    </td>';
            }
            else
            {
                qty_target = 0;
                td_str += '<td >\
                    <input '+input_attr+' data-type="target" value="">\
                    </td>';
            }
            
            
            if(/*obj[x]['details'][y]['qty_confirmed'] */ qty_confirmed != null && qty_confirmed != '')
            {
                qty_confirmed = parseFloat(qty_confirmed);
                td_str += '<td class="currency"> \
                    <input '+input_attr+' data-type="confirmed" value="'+qty_confirmed.toLocaleString()+'">\
                    </td>';
            }                
            else
            {
                qty_confirmed = 0;
                td_str += '<td>\
                    <input '+input_attr+' data-type="confirmed" value="">\
                    </td>';
            }
                
            
            if(qty_projection != null && qty_projection != '')
            {
                qty_projection = parseFloat(qty_projection);
                td_str += '<td>\
                    <input '+input_attr+' data-type="projection" value="'+qty_projection.toLocaleString()+'"></td>';
            }
            else
            {
                qty_projection = 0;
                 td_str += '<td>\
                    <input '+input_attr+' data-type="projection" value=""></td>';
            }
            
            if(qty_previous != null && qty_previous != '')
            {
                qty_previous = parseFloat(qty_previous);
                td_str += '<td> \
                    <input '+input_attr+' data-type="previous" value="'+qty_previous.toLocaleString()+'">\
                    </td>';            
            }
            else
            {
                qty_previous = 0;
                td_str += '<td> \
                    <input '+input_attr+' data-type="previous" value="">\
                    </td>';
            }
           
            
            qty_confirmed_sub_totals[y] == undefined ? qty_confirmed_sub_totals[y] = qty_confirmed : qty_confirmed_sub_totals[y] += qty_confirmed;
            qty_previous_sub_totals[y] == undefined ? qty_previous_sub_totals[y] = qty_previous : qty_previous_sub_totals[y] += qty_previous;
            qty_projection_sub_totals[y] == undefined ? qty_projection_sub_totals[y] = qty_projection : qty_projection_sub_totals[y] += qty_projection;
            qty_target_sub_totals[y] == undefined ? qty_target_sub_totals[y] = qty_target : qty_target_sub_totals[y] += qty_target;
            
        }
        
        td_str += '</tr>';
    }
    
    td_str += '<tr><td>Sub Total</td>';
    for(y=0 ; y < month_count ; y++)
    {        
        td_str += '<td class="currency">'+qty_target_sub_totals[y].toLocaleString()+'</td>\
            <td  class="currency">'+qty_confirmed_sub_totals[y].toLocaleString()+'</td>\
            <td class="currency">'+qty_projection_sub_totals[y].toLocaleString()+'</td> \
            <td class="currency">'+qty_previous_sub_totals[y].toLocaleString()+'</td>';       
    }
    td_str += '<tr>';
    
    td_str += '<tr><td>Total Confirmed</td>';
    for(y=0 ; y < month_count ; y++)
    {        
        td_str += '<td class="currency" colspan="4">'+qty_confirmed_sub_totals[y].toLocaleString()+'</td>';       
    }
    td_str += '<tr>';
    
    td_str += '<tr><td>Total Target</td>';
    for(y=0 ; y < month_count ; y++)
    {        
        td_str += '<td class="currency" colspan="4">'+qty_target_sub_totals[y].toLocaleString()+'</td>';       
    }
    td_str += '<tr>';
    
    tbl.append(td_str);
    $('#04-sales-plan-table').append(tbl);
    
    waitingDialog.hide();

        }
    });

});


$('#04-btn-save').click(function(){
   /* var plan = [];
    var inputs = $('#04-sales-plan-table input[type=text]');
    inputs.each(function() {
       ele = $( this );
       plan.push({
           'buyer' : ele.attr('data-buyer'),
           'month' : ele.attr('data-month'),
           'type' : ele.attr('data-type'),
           'qty' : ele.val()
       });
    });*/
    waitingDialog.show('Saving...', {dialogSize: 'sm', progressType: 'primary'}); 
    
    var plan = [];
    var months = [];
    var trs = $('#04-sales-plan-table tbody tr');
    trs.each(function(){           
        var inputs = $(this).find('input');
        if(inputs.length > 0)
        {
            var buyer = inputs[0].getAttribute('data-buyer');            
            var count = inputs.length;
            for(var x = 0 ; x < count ; x += 4)
            {
                var confirmed = inputs[x+1].value.replace(/,/g, '');
                var projection = inputs[x+2].value.replace(/,/g, '');
                var previous = inputs[x+3].value.replace(/,/g, '');
                var target = inputs[x].value.replace(/,/g, '');
                var month = inputs[x].getAttribute('data-month');
                months.push(month);
                
                if(confirmed == '')
                    confirmed = 0;
                if(projection == '')
                    projection = 0;
                if(previous == '')
                    previous = 0;
                if(target == '')
                    target = 0;
                
                var obj = {
                   'buyer' : buyer,
                   'month' : month,
                   'target' : target,
                   'confirmed' : confirmed,// inputs[x+1].value,
                   'projection' : projection,// inputs[x+2].value,
                   'previous' : previous// inputs[x+3].value
               };
               plan.push(obj);
            }  
        }    
    });
    
    make_ajax_request({
        url : BASE_URL+'index.php/sales_plan/save_sales_plan',
        data : { 'plan' : plan , 'months' : months},
        success : function(response){
            //waitingDialog.hide();
            $('#04-btn-search').click();
        }
    });
  
});


$('#04-sales-plan-table input').on('blur',function(){
 //   this.value = this.value.toLocaleString();
});

/*
$('#04-btn-search').click(function(){
    
    waitingDialog.show('Custom message', {dialogSize: 'sm', progressType: 'warning'});
    
    var fdate = $('#04-from-date').val();
    var tdate = $('#04-to-date').val();
    var sub_totals = [];
    
    var obj = make_ajax_request({
        url : BASE_URL+'index.php/sales_plan/get_sales_plan',
        async : false,
        data : {'from_date' : fdate , 'to_date' : tdate}
    });
    var len = obj.length;
    var month_count = obj[0]['details'].length;
    
    var thead = $('#04-sales-plan-table thead');
    thead.empty();
    var thead_str = '<tr style="background: #B5D1D8"> <th>Month</th>';
    for(y=0 ; y < month_count ; y++)
    {        
        thead_str += '<th style="text-align: center;" colspan="3">'+obj[0]['details'][y]['month']+'</th>';       
    }
    thead_str += '</tr>';
    thead.append(thead_str);
    
    var tbl = $('#04-sales-plan-table tbody');
    tbl.empty();
    var tbl_header = $('<tr></tr>');
    tbl_header.append('<td>Buyer</td>');
    tbl.append(tbl_header);
    var th_str = '';
    
    for(y=0 ; y < month_count ; y++)
    {
        th_str += '<td class="title">Target</td><td class="title">Confirmed</td><td class="title">Projection</td>';
    }
    tbl_header.append(th_str);
    var td_str = '';
    for(x = 0 ; x < len ; x++)
    {
        td_str += '<tr> <td>'+obj[x]['buyer']+'</td>';
        
        for(y=0 ; y < month_count ; y++)
        {
            var qty_total = 0;
            if(obj[x]['details'][y]['qty'] != null)
            {
                qty_total = parseFloat(obj[x]['details'][y]['qty']);
                td_str += '<td></td><td class="currency">$'+obj[x]['details'][y]['qty']+'</td><td></td>';
            }                
            else
                td_str += '<td></td><td></td><td></td>';
            
            if(sub_totals[y] == undefined)
                sub_totals[y] = 0;
            else
                sub_totals[y] += qty_total;
        }
        
        td_str += '</tr>';
    }
    
    td_str += '<tr><td>Sub Total</td>';
    for(y=0 ; y < month_count ; y++)
    {        
        td_str += '<td></td><td  class="currency">$'+sub_totals[y]+'</td><td></td>';       
    }
    td_str += '<tr>';
    
    td_str += '<tr><td>Total Confirmed</td>';
    for(y=0 ; y < month_count ; y++)
    {        
        td_str += '<td class="currency" colspan="3">$0</td>';       
    }
    td_str += '<tr>';
    
    td_str += '<tr><td>Total Target</td>';
    for(y=0 ; y < month_count ; y++)
    {        
        td_str += '<td class="currency" colspan="3">$0</td>';       
    }
    td_str += '<tr>';
    
    tbl.append(td_str);
    $('#04-sales-plan-table').append(tbl);
    
    waitingDialog.hide();
});*/




