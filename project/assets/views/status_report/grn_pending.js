
$(document).ready(function(){
    
    $('#gp-list').DataTable( {
        "scrollY": "400px",
        "scrollX": true,
        //"ordering": false,
        "searching": false,
        //"scrollCollapse": true,
       // responsive: true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": PAGE.getBaseUrl()+"index.php/status_report/get_pending_grn",
            'data' : {
                'buyer' : PAGE.getBuyer(),
                'date' : PAGE.getDate(),
                'factory_code' : PAGE.getFactoryCode()
            }
            //"type": "POST"
        },
        "columns": [
            { "data": "PART_NO" ,width : '40%'},
            { "data": "ITEM_NO" }  ,
            { "data": "ITEM_DESC" }  ,
            { "data": "UOM" }  ,
            { "data": "QTY_REQUIRED" },
            { "data": "PROD_FAMILY" },
            { "data": "STYLE" }, 
            { "data": "BUYER_CODE" },
            { "data": "MERCHAND" } ,
            { "data": "PO_NO" } ,
            { "data": "VENDOR" , width : '20%'},
            { "data": "CUSTOMER_ORDER_NO" },
            { "data": "LOT" },
            { "data": "C_PCD" },
            { "data": "WORK_CENTER_NO" } 
        ]
    } );
    
});


