
$(document).ready(function(){
    
    var now = new Date();
    var end = new Date();
    end.setDate(now.getDate() + 6);
    var daysOfYear = [];
    
    for (var d = new Date(); d <= end; d.setDate(d.getDate() + 1)) {
       daysOfYear.push(d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear());
    }
    
    var thead = $('#status-report-table thead');
    var thead_str = '';
    thead_str += '<tr><th class="headcol" style="min-width:100px">PCD Date</th>';
    for(var xx = 0 ; xx < daysOfYear.length ; xx++){
    //for (var d = new Date(); d <= end; d.setDate(d.getDate() + 1)) {
        thead_str += '<th colspan="5" style="text-align:center">'+daysOfYear[xx]+'</th>';  
    }
    thead_str += '</tr>';
    //thead.append(thead_str);
    
    var now_date = now.getFullYear()+'-'+(now.getMonth()+1)+'-'+now.getDate();
    var end_date = end.getFullYear()+'-'+(end.getMonth()+1)+'-'+end.getDate();

    make_ajax_request({
        url : BASE_URL+'index.php/status_report/get_status_report',
        data : { 'start' : now_date , 'end' : end_date },
        async : false,
        success : function(response){
            try{
                var obj = JSON.parse(response);
                var buyers = obj['buyers'];
                thead_str += '<tr><th class="headcol">Factory</th>';
                
                ///for (var d = new Date(); d <= end; d.setDate(d.getDate() + 1)) {
                for(var xx = 0 ; xx < daysOfYear.length ; xx++){
                    for(var x = 0 ; x < buyers.length ; x++)
                    {
                        thead_str += '<th style="min-width:100px">'+buyers[x]['buyer_code']+'</th>';
                    }
                } 
                thead_str += '</tr>';
                thead.append(thead_str);
                
                var report = obj['report'];
                var tbody_str = '';
                var tdCount = 1;
                for(var x = 0 ; x < report.length ; x++)
                {
                    tbody_str += '<tr>';
                    tbody_str += '<td class="headcol">'+report[x]['factory']+'</td>';
                    var data = report[x]['data'];
                    for(var y = 0 ; y < data.length ; y++)
                    {
                        var ds = data[y]['status'];
                        for(var key in ds)
                        {
                            //alert(ds[key]);
                            tbody_str += '<td id="'+tdCount+'" data-factory-code="'+report[x]['factory_code']+'" data-buyer="'+key+'" data-date="'+data[y]['date']+'" style="cursor:pointer">'+''/*ds[key]*/+'</td>';
                            /*if(ds[key] == 0)
                                tbody_str += '<td id="'+tdCount+'" data-factory-code="'+report[x]['factory_code']+'" data-buyer="'+key+'" data-date="'+data[y]['date']+'" style="background-color:#c2f0c2;cursor:pointer">'+ds[key]+'</td>';
                            else
                                tbody_str += '<td id="'+tdCount+'" data-factory-code="'+report[x]['factory_code']+'" data-buyer="'+key+'" data-date="'+data[y]['date']+'" style="background-color:#ff9999;cursor:pointer">'+ds[key]+'</td>';
                            */
                            tdCount++;
                        }
                    }            
                    tbody_str += '</tr>';
                }
                
                var tbody = $('#status-report-table tbody');
                tbody.append(tbody_str);
            }
            catch(e)
            {
                
            }
        }
    });
    
    
    var td_list = $('#status-report-table tbody td');
    var ll = td_list.length;
   // td_list.each(function(){
   for(var z = 0 ; z < ll ; z++){
        var id = td_list[z].getAttribute('id');// this.getAttribute('id');
        var buyer = td_list[z].getAttribute('data-buyer');//this.getAttribute('data-buyer');
        var date = td_list[z].getAttribute('data-date');//this.getAttribute('data-date');
        var factory = td_list[z].getAttribute('data-factory-code');//this.getAttribute('data-factory-code'); 
        
        if(id != undefined)
        {
           load_report(id,buyer,date,factory)
            /*make_ajax_request({
                url : BASE_URL+'index.php/status_report/get_grn_count',
                data : {'buyer' : buyer , 'date' : date , 'factory' : factory},
                async : false,
                success : function(response){
                    try{
                        var obj = JSON.parse(response);
                        if(obj['grn_count'] != null && obj['grn_count'] != undefined)
                        {
                            if(obj['grn_count'] == 0)
                                $('#'+id).css('background-color','#c2f0c2').html(obj['grn_count']);
                            else
                                $('#'+id).css('background-color','#ff9999').html(obj['grn_count']);
                        }
                    }
                    catch(e){}
                }
            });
            setTimeout(function(){},1000);*/
        }
    }
    
});


function load_report(id,buyer,date,factory){
    make_ajax_request({
                url : BASE_URL+'index.php/status_report/get_grn_count',
                data : {'buyer' : buyer , 'date' : date , 'factory' : factory},
                async : true,
                success : function(response){
                    try{
                        var obj = JSON.parse(response);
                        if(obj['grn_count'] != null && obj['grn_count'] != undefined)
                        {
                            if(obj['pcd_count'] == 0)
                            {
                                $('#'+id).css('background-color','#f2f2f2').html();
                            }
                            else
                            {
                                if(obj['grn_count'] == 0)
                                    $('#'+id).css('background-color','#c2f0c2').html(obj['grn_count']);
                                else
                                    $('#'+id).css('background-color','#ff9999').html(obj['grn_count']);
                            }                            
                        }
                    }
                    catch(e){}
                }
            });
}



$('#status-report-table').on('click','td',function(){
    var ele = $(this);
    if(ele.attr('id') != undefined && ele.html() != "")
    {
       var buyer = ele.attr('data-buyer');
        var date = ele.attr('data-date');
        var factory = ele.attr('data-factory-code');
        window.open(BASE_URL+'index.php/status_report/grn_pending/'+buyer+'/'+date+'/'+factory);
        //alert($(this).attr('data-buyer'));     
    }    
});