var TABLE = null;
var MSG = null;

$(document).ready(function() {
    
    MSG = $("body").messageBox({
        modal:true,
       // cbClose : delete_user_process,
        autoClose: 0,
        locale:{
            NO : 'No',
            YES : 'Yes1',
            CANCEL : 'Cancel',
            OK : 'OK'//,
            //textAutoClose: 'Auto close in %d seconds'
          }
    });
    
    
    TABLE = $('#06-users-table').DataTable( {
        "scrollY": "500px",
        "scrollCollapse": true,
        responsive: true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL+"index.php/user/get_users"
            //"type": "POST"
        },
        "columns": [
            { "data": "id" },
            { "data": "first_name" },
            { "data": "last_name" },
            { "data": "user_name" },
            { "data": "level_name" },
            { "data": "contact_no" },
            { "data": "email" },
            {
                "orderable": false,
                'data' : function(_data,a){
                    return '<button class="btn btn-success btn-flat btn-sm" data-id="'+_data['id']+'" onclick="edit_user(this);">Edit</button>';
                }
            },
            {
                "orderable": false,
                data : function(_data){
                    return '<button class="btn btn-danger btn-flat btn-sm" data-id="'+_data['id']+'" onclick="delete_user(this)">Delete</button>';
                }
            }
        ]
    } );
} );


function edit_user(ele)
{
    //alert($(ele).attr('data-id'));
    window.open(BASE_URL+'index.php/user/open_user/'+$(ele).attr('data-id'));
}


function delete_user(ele)
{
    var ans = confirm('Are you sure you want to delete this user?');
    //var ans = MSG.data('messageBox').question('Delete User', 'Do you want to delete this user?', [{text:'Yes',return:$(ele).attr('data-id')}, {text:'No',return:false}]);
    if(ans == true)
    {
        make_ajax_request({
            url : BASE_URL+'index.php/user/delete_user/'+$(ele).attr('data-id'),
            success : function(response){
                try{
                    var obj = JSON.parse(response);
                    //alert(obj['message']);
                    MSG.data('messageBox').info('Success', obj['message']);
                   TABLE.ajax.reload( null, false );
                }
                catch(e)
                {  
                    MSG.data('messageBox').danger('Error', e);
                }              
            }
        });
    }
}


function delete_user_process(ret)
{
    if(ret != 'false')
    {
        make_ajax_request({
            url : BASE_URL+'index.php/user/delete_user/'+ret,
            success : function(response){
                try{
                    var obj = JSON.parse(response);
                    //alert(obj['message']);
                    MSG.data('messageBox').info('Success', obj['message'],[{text:'OK',return:false}]);
                   TABLE.ajax.reload( null, false );
                }
                catch(e)
                {  
                    MSG.data('messageBox').danger('Error', e);
                }              
            }
        });
    }
}