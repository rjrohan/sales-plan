
$(document).ready(function(){
    
    var id = PAGE.getUserId();
    
    $('#ua-form').form_validator({
        events : ['blur'],
        fields : {
            'ua-first-name' : {
                key : 'first_name',
                notEmpty : {
                    message : 'First name cannot be empty'
                }
            },
            'ua-last-name' : {
                key : 'last_name',
                notEmpty : {
                    message : 'Last name cannot be empty'
                }
            },
            'ua-contact-no' : {
                key : 'contact_no',
                /*notEmpty : {
                    message : 'Contact number cannot be empty'
                },*/
                notRequired : true,
                type : {
                    type : 'phone',
                    message : 'Incorrect contact number'
                }
            },
            'ua-email' : {
                key : 'email',
                notEmpty : {
                    message : 'Email address cannot be empty'
                },
                notRequired : true,
                type : {
                    type : 'email',
                    message : 'Incorrect email address'
                },
                remote : {
                    url : PAGE.getBaseUrl()+'index.php/user/is_email_exists',
                    data : {value: function(){ return $('#ua-email').val(); } , id:id}
                }
            },               
            'ua-user-level' : {
                key : 'user_level',
                notRequired : true
            },
            'ua-department' : {
                key : 'department',
                notEmpty : {
                    message : 'Department cannot be empty'
                },
            },
            'ua-password' : {
                key : 'password',
                notRequired : true
            },
            'ua-conf-password' : {
                key : 'conf_pass',
                notRequired : true,
                valueCheck : {
                    'ele_to_check' : 'ua-password',
                    message : 'Confirm your password'
                }
            }
        }
    });  
    
    load_user_details(id);
    
});


$('#ua-btn-save').click(function(){
    var obj =  $('#ua-form').form_validator('validate');   
    if(obj !== undefined && obj !== false)
    {
       waitingDialog.show('Saving...', {dialogSize: 'sm', progressType: 'primary'});
        var user_id = PAGE.getUserId();
        obj['id'] = user_id;
        obj['user_name'] = obj['email'];
       make_ajax_request({
           url : PAGE.getBaseUrl()+'index.php/user/save_user',
           'data' : obj,
           'success' : function(response){
               try{
                   var res = JSON.parse(response);
                   if(res['status'] == true)
                   {
                       if(user_id == 0)
                       {
                            jsSetFormData(
                                [
                                   { id : "ua-first-name" , value : ""},
                                   { id : "ua-last-name" , value : ""},
                                   { id : "ua-contact-no" , value : ""},
                                   { id : "ua-email" , value : ""},
                                   //{ id : "ua-username" , value : ""},
                                   { id : "ua-password" , value : ""},
                                   { id : "ua-conf-password" , value : ""}
                                ]
                            );
                       }
                       $('#ua-success-aler').show().find('span').html(res['message']);
                       setTimeout(function(){
                           waitingDialog.hide();
                       },1000)                       
                   }    
                   else
                   {
                       waitingDialog.hide();
                       alert(res['message']);
                   }
                       
               }
               catch(e)
               {
                   alert(e);
               }
              
           }
       });
    }         
    else
     return false;
});


function load_user_details(user_id)
{
    waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'primary'});
    make_ajax_request({
        url : PAGE.getBaseUrl()+'index.php/user/get_user',
        data : { id : user_id},
        success : function(response){
            try{
                var obj = JSON.parse(response);
                if(obj != undefined && obj != null)
                jsSetFormData(
                    [
                        {id : 'ua-first-name' , value : obj['first_name']},
                        {id : 'ua-last-name' , value : obj['last_name']},
                        {id : 'ua-contact-no' , value : obj['contact_no']},
                        {id : 'ua-email' , value : obj['email']},
                       // {id : 'ua-username' , value : obj['user_name']},
                        {id : 'ua-user-level' , value : obj['user_level']},
                        {id : 'ua-department' , value : obj['department']}
                    ]
                );
        
                setTimeout(function(){
                    waitingDialog.hide();
                },1000)
            }
            catch(e)
            {
                console.log(e);
            }            
        }
    });
}


$('#ua-email').keyup(function(){
    $('#ua-username').val(this.value);
});