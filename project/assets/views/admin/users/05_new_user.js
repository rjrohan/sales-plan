var MSG = null;

$(document).ready(function(){
    
    MSG = $("body").messageBox({
        modal:true,
        //cbClose : msgClose,
        autoClose: 0,
        locale:{
            NO : 'No',
            YES : 'Yes',
            CANCEL : 'Cancel',
            OK : 'OK'//,
            //textAutoClose: 'Auto close in %d seconds'
          }
    });
    
    var id = $('#05-user-id').val();
    
    var validate_obj = {
        events : ['blur'],
        fields : {
            '05-first-name' : {
                key : 'first_name',
                notEmpty : {
                    message : 'First name cannot be empty'
                }
            },
            '05-last-name' : {
                key : 'last_name',
                notEmpty : {
                    message : 'Last name cannot be empty'
                }
            },
            '05-contact-no' : {
                key : 'contact_no',
                /*notEmpty : {
                    message : 'Contact number cannot be empty'
                },*/
                notRequired : true,
                type : {
                    type : 'phone',
                    message : 'Incorrect contact number'
                }
            },
            '05-email' : {
                key : 'email',
                notEmpty : {
                    message : 'Email address cannot be empty'
                },
                notRequired : true,
                type : {
                    type : 'email',
                    message : 'Incorrect email address'
                },
                remote : {
                    url : BASE_URL+'index.php/user/is_email_exists',
                    data : {value: function(){ return $('#05-email').val(); } , id:id}
                }
            },
            /*'05-username' : {
                key : 'username',
                notEmpty : {
                    message : 'Username cannot be empty'
                },
                remote : {
                    url : BASE_URL+'index.php/user/is_username_exists',
                    data : {value: function(){ return $('#05-username').val(); } , id:id}
                }
            }, */   
            '05-permission-group' : {
                key : 'permission_group',
                notRequired : true
            },
            '05-user-level' : {
                key : 'user_level',
                notRequired : true
            },
            '05-department' : {
                key : 'department',
                notEmpty : {
                    message : 'Department cannot be empty'
                },
            }
        }
    };
    
    if(id != '0')
    {
       $('#05-title').html('Update User Details');
       
       validate_obj['fields']['05-password'] = {
                key : 'password',
                notRequired : true
            };
        validate_obj['fields']['05-conf-password'] = {
                key : 'conf_pass',
                notRequired : true,
                valueCheck : {
                    'ele_to_check' : '05-password',
                    message : 'Confirm your password'
                }
            };
       
       load_user_details(id);
    }       
    else
    {
        validate_obj['fields']['05-password'] = {
                key : 'password',
                notEmpty : {
                    message : 'Password cannot be empty'
                }
            };
        validate_obj['fields']['05-conf-password'] = {
                key : 'conf_pass',
                notEmpty : {
                    message : 'Confirm passwor cannot be empty'
                },
                valueCheck : {
                    'ele_to_check' : '05-password',
                    message : 'Confirm your password'
                }
            };
    }
     //initialize form validation plugin
    $('#05-form').form_validator(validate_obj);  
    
});


$('#05-btn-save').click(function(){
    var obj =  $('#05-form').form_validator('validate');   
    if(obj !== undefined && obj !== false)
    {
       //waitingDialog.show('Saving...', {dialogSize: 'sm', progressType: 'primary'});
        var user_id = $('#05-user-id').val();
        obj['id'] = user_id;
        obj['user_name'] = obj['email'];
       make_ajax_request({
           url : BASE_URL+'index.php/user/save_user',
           'data' : obj,
           async : false,
           'success' : function(response){
               try{
                   var res = JSON.parse(response);
                   if(res['status'] == true)
                   {
                       if(user_id == 0)
                       {
                            jsSetFormData(
                                [
                                   { id : "05-first-name" , value : ""},
                                   { id : "05-last-name" , value : ""},
                                   { id : "05-contact-no" , value : ""},
                                   { id : "05-email" , value : ""},
                                   //{ id : "05-username" , value : ""},
                                   { id : "05-password" , value : ""},
                                   { id : "05-conf-password" , value : ""}                                   
                                ]
                            );
                       }
                       $('#05-success-aler').show().find('span').html(res['message']);
                      /* setTimeout(function(){
                           waitingDialog.hide();
                       },1000) ;*/
                       MSG.data('messageBox').info('Success', res['message']);
                   }    
                   else
                   {
                       //waitingDialog.hide();
                       //alert(res['message']);
                       MSG.data('messageBox').danger('Error', res['message']);
                   }
                       
               }
               catch(e)
               {
                   alert(e);
               }
              
           }
       });
    }         
    else
     return false;
});


function load_user_details(user_id)
{
    waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'primary'});
    make_ajax_request({
        url : BASE_URL+'index.php/user/get_user',
        data : { id : user_id},
        success : function(response){
            try{
                var obj = JSON.parse(response);
                if(obj != undefined && obj != null)
                jsSetFormData(
                    [
                        {id : '05-first-name' , value : obj['first_name']},
                        {id : '05-last-name' , value : obj['last_name']},
                        {id : '05-contact-no' , value : obj['contact_no']},
                        {id : '05-email' , value : obj['email']},
                       // {id : '05-username' , value : obj['user_name']},
                        {id : '05-user-level' , value : obj['user_level']},
                        {id : '05-department' , value : obj['department']},
                        { id : "05-permission-group" , value : obj['permission_group']}
                    ]
                );
        
                setTimeout(function(){
                    waitingDialog.hide();
                },1000)
            }
            catch(e)
            {
                console.log(e);
            }            
        }
    });
}


$('#05-email').keyup(function(){
    $('#05-username').val(this.value);
});