$(document).ready(function(){
    
    var msg = $("body").messageBox({
        modal:true,
        //cbClose : msgClose,
        autoClose: 0,
        locale:{
            NO : 'No',
            YES : 'Yes',
            CANCEL : 'Cancel',
            OK : 'OK'//,
            //textAutoClose: 'Auto close in %d seconds'
          }
    });
    PAGE.setMessageBox(msg);
    
    $('#pg-form').form_validator({
        events : ['blur'],
        fields : {
            'pg-group-name' : {
                key : 'group_name',       
                notEmpty : {
                message : 'Permission group name cannot be empty'
                }
            }
        }
    });
    
    if(PAGE.getGroupId() == 0)
    {
        make_ajax_request({
            url : PAGE.getBaseUrl()+'index.php/permission_group/get_menu_list',
            success : function(response){
                try{
                    var obj = JSON.parse(response);
                    load_permissions(obj);
                }
                catch (e){
                    
                }
            }
        });        
    }
    else
    {
      load_permission_group();  
    }
    
});


function load_permission_group()
{
    make_ajax_request({
        url : PAGE.getBaseUrl()+'index.php/permission_group/get_permissions_group',
        data : {'group_id' : PAGE.getGroupId()},
        success : function(response){
            try{
                var obj = JSON.parse(response);
                if(obj != undefined && obj != null)
                {
                    $('#pg-group-name').val(obj['group_details']['group_name']);
                    load_permissions(obj['group_permissions']);
                }
            }
            catch(e){
                
            }
        }
    });
}


function load_permissions(per_arr)
{
    if(per_arr != null || per_arr != undefined)
    {
        var ele = $('#pg-permission-list tbody');
        ele.empty();
        var str = '';
        for(var x = 0 ; x < per_arr.length ; x++)
        {
            var permission = per_arr[x];
            str += '<tr>';
            /*if(permission['permission_status'] == undefined || permission['permission_status'] == '0')
                str += '<td><input data-permission-id="'+permission['permission_id']+'" type="checkbox"></td><td>'+permission['permission']+'</td>';
            else if(permission['permission_status'] == '1')
                str += '<td><input data-permission-id="'+permission['permission_id']+'" type="checkbox" checked></td><td>'+permission['permission']+'</td>';
            str += '</tr>';*/
             if(permission['permission_status'] == undefined || permission['permission_status'] == '0')
                str += '<td><input data-permission-id="'+permission['menu_id']+'" type="checkbox"></td><td>'+permission['menu_text']+'</td>';
            else if(permission['permission_status'] == '1')
                str += '<td><input data-permission-id="'+permission['menu_id']+'" type="checkbox" checked></td><td>'+permission['menu_text']+'</td>';
        }
        ele.html(str);
    }
}


function get_permissions()
{
    var chk_boxes = $('#pg-permission-list tbody input[type = checkbox]');
    var arr = [];
    chk_boxes.each(function(){
        var ele = $(this);
        arr.push({
            menu_id : ele.attr('data-permission-id'),
            permission_status : ele.prop('checked') == true ? 1 : 0
        });
    });
    return arr;
}


$('#pg-btn-save').click(function(){
    
    var obj = $('#pg-form').form_validator('validate');
    if(obj != false)
    {
        make_ajax_request({
            url : PAGE.getBaseUrl()+'index.php/permission_group/save_permission_group',
            data : {
                'group_id' : PAGE.getGroupId() , 
                'group_name' : obj['group_name'],
                'permissions' : get_permissions()
            },
            async : false,
            success : function(response){
                try{
                    var res = JSON.parse(response);
                    if(res['status'] == true)
                    {
                        PAGE.getMessageBox().data('messageBox').info('Success', res['message']);
                        setTimeout(function(){
                            location.reload();
                        },1000);                        
                    }
                    else{
                        PAGE.getMessageBox().data('messageBox').danger('Error', res['message']);
                    }
                    //alert(res['message']);               
                }
                catch(e){alert(e);}
            }
        });
    }   
    
});

