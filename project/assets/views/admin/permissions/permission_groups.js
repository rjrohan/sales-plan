$(document).ready(function() {
    
    var msg = $("body").messageBox({
        modal:true,
        //cbClose : msgClose,
        autoClose: 0,
        locale:{
            NO : 'No',
            YES : 'Yes',
            CANCEL : 'Cancel',
            OK : 'OK'//,
            //textAutoClose: 'Auto close in %d seconds'
          }
    });
    PAGE.setMessageBox(msg);
    
    var tbl = $('#pg-groups-table').DataTable( {
        "scrollY": "500px",
        "scrollCollapse": true,
        responsive: true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": PAGE.getBaseUrl()+"index.php/permission_group/get_permissions_groups"
            //"type": "POST"
        },
        "columns": [
            { "data": "group_id" , width : '10%' },
            { "data": "group_name" },      
            {
                "orderable": false,
                width : '10%',
                'data' : function(_data,a){
                    return '<button class="btn btn-success btn-flat btn-sm" data-group-id="'+_data['group_id']+'" onclick="edit_group(this);">Edit</button>';
                }
            },
            {
                "orderable": false,
                width : '10%',
                data : function(_data){
                    return '<button class="btn btn-danger btn-flat btn-sm" data-group-id="'+_data['group_id']+'" onclick="delete_group(this)">Delete</button>';
                }
            }
        ]
    } );
    PAGE.setTable(tbl);
} );


function edit_group(ele)
{
    //alert($(ele).attr('data-id'));
    window.open(PAGE.getBaseUrl()+'index.php/permission_group/open_permission_group/'+$(ele).attr('data-group-id'));
}


function delete_group(ele)
{
    var ans = confirm('Are you sure you want to delete this permission group?');
    if(ans == true)
    {
        make_ajax_request({
            url : PAGE.getBaseUrl()+'index.php/permission_group/delete_permission_group/'+$(ele).attr('data-group-id'),
            async : false,
            success : function(response){
                try{
                    var obj = JSON.parse(response);
                    if(obj['status'] == true)
                    {
                       PAGE.getMessageBox().data('messageBox').info('Success', obj['message']);
                       PAGE.getTable().ajax.reload( null, false ); 
                    }
                    else
                    {
                        PAGE.getMessageBox().data('messageBox').danger('Error', obj['message']);
                    }
                    //alert(obj['message']);                   
                }
                catch(e)
                {                    
                }              
            }
        });
    }
}