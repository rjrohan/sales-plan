var TABLE = null;
var MSG = null;

$(document).ready(function(){
    
    MSG = $("body").messageBox({
        modal:true,
        //cbClose : msgClose,
        autoClose: 0,
        locale:{
            NO : 'No',
            YES : 'Yes',
            CANCEL : 'Cancel',
            OK : 'OK'//,
            //textAutoClose: 'Auto close in %d seconds'
          }
    });
    
    TABLE = $('#gp-list').DataTable( {
        "scrollY": "500px",
        "scrollCollapse": true,
       // responsive: true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL+"index.php/gate_pass/get_gate_passes"
            //"type": "POST"
        },
        "columns": [
            { "data": "id" },
            { "data": "gp_no" },
            { "data": "gp_to_address" },
            { "data": "date" },
            //{ "data": "style" },
            { "data": "attention" }, 
            //{ "data": "through" }, 
            //{ "data": "remark" }, 
            { "data": "instructed_by" }, 
            { "data": "type" }, 
            //{ "data": "special_instruction" }, 
            {
                "orderable": false,
                'data' : 'approve',
                render : function(data, type, full, meta){
                    if(data == 'pending')
                        return '<button class="btn btn-warning btn-flat btn-sm" style="width:120px" data-id="'+data+'">'+data+'</button>';
                    else if(data == 'approval pending')
                        return '<button class="btn btn-info btn-flat btn-sm" style="width:120px" data-id="'+data+'">'+data+'</button>';
                    else if(data == 'approved')
                        return '<button class="btn btn-success btn-flat btn-sm" style="width:120px" data-id="'+data+'">'+data+'</button>';
                    else if(data == 'rejected')
                        return '<button class="btn btn-danger btn-flat btn-sm" style="width:120px" data-id="'+data+'" >'+data+'</button>';
                }
            },
            {
                "orderable": false,
                'data' : 'id',
                "render": function ( data, type, full, meta ){
                    return '<button class="btn btn-success btn-flat btn-sm" data-id="'+data+'" onclick="edit_gate_pass(this);">Edit</button>';
                }
            },
            {
                "orderable": false,
                'data' : 'id',
                render : function(data, type, full, meta){
                    return '<button class="btn btn-danger btn-flat btn-sm" data-id="'+data+'" onclick="delete_gate_pass(this)">Delete</button>';
                }
            }            
        ]
    } );
    
});


function edit_gate_pass(ele)
{
    window.open(BASE_URL+'index.php/gate_pass/open_gate_pass/'+$(ele).attr('data-id'));
}

function delete_gate_pass(ele)
{
    var ans = confirm('Do you want to delete this gate pass?');
    if(ans == false)
        return;
    
    var id = $(ele).attr('data-id');
    make_ajax_request({
        url : BASE_URL+'index.php/gate_pass/delete_gate_pass',
        data : { id : id},
        async : false,
        success : function(response){
            try{
                var obj = JSON.parse(response);
                if(obj['status'] == true)
                {
                    //alert(obj['message']);
                    MSG.data('messageBox').info('Success', obj['message']);
                    TABLE.ajax.reload(null , false);
                }
                else
                {
                    MSG.data('messageBox').danger('Error', res['message']);
                    //alert(obj['message']);
                }                    
            }
            catch(e){   
                alert(e);
            }
        }
    });
}



$('#gp-box-body').bind('resize', function(){
     alert();
});