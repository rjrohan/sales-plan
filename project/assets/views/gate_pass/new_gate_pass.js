
var ITEMS = [
    /*{'line_no':1,'description':'ddddddddd11 ','unit':'kg','qty' : 100,'status':'returnable'},
     {'line_no':2,'description':'ddddddddd22','unit':'kg','qty' : 100,'status':'non returnable'},
      {'line_no':3,'description':'ddddddddd33','unit':'kg','qty' : 100,'status':'returnable'},
      {'line_no' :4,'description':'ddddddddd11 ','unit':'kg','qty' : 100,'status':'returnable'},
     {'line_no':5,'description':'ddddddddd22','unit':'kg','qty' : 100,'status':'non returnable'},
      {'line_no':6,'description':'ddddddddd33','unit':'kg','qty' : 100,'status':'returnable'},
      {'line_no':7,'description':'ddddddddd11 ','unit':'kg','qty' : 100,'status':'returnable'},
     {'nline_no':8,'description':'ddddddddd22','unit':'kg','qty' : 100,'status':'non returnable'},
      {'line_no':9,'description':'ddddddddd33','unit':'kg','qty' : 100,'status':'returnable'},
      {'line_no':10,'description':'ddddddddd33','unit':'kg','qty' : 100,'status':'returnable'}*/
];

var TABLE = null;
var GATE_PASS_HEADER = null;
var GATE_PASS_USER = null;

var MSG = null;

$(document).ready(function() {
    
    /*$('#gp-date').datepicker({
        format: 'yyyy-mm-dd'
    });*/
    
    MSG = $("body").messageBox({
        modal:true,
        //cbClose : msgClose,
        autoClose: 0,
        locale:{
            NO : 'No',
            YES : 'Yes',
            CANCEL : 'Cancel',
            OK : 'OK'//,
            //textAutoClose: 'Auto close in %d seconds'
          }
    });
    
    $('#gp-form').form_validator({
        events : ['blur'],
        fields : {
            'gp-no' : {
                key : 'gp_no',
                notEmpty : {
                    message : 'Gate pass number cannot be empty'
                }
            },
            'gp-to' : {
                key : 'gp_to_address',
                notEmpty : {
                    message : 'gate pass to cannot be empty'
                }
            },
            'gp-attention' : {
                key : 'attention',
                notEmpty : {
                    message : 'Attention field cannot be empty'
                }
            },
            'gp-remark' : {
                key : 'remark',
                notRequired : true
            },
            'gp-date' : {
                key : 'date',
                notEmpty : {
                    message : 'Date cannot be empty'
                }
            },           
            'gp-ref' : {
                key : 'style',
                notRequired : true
            },
            'gp-through' : {
                key : 'through',
                notRequired : true
            },
            'gp-instructed-by' : {
                key : 'instructed_by',
                notEmpty : {
                    message : 'Instructed by field cannot be empty'
                }
            },
            'gp-special-instruction' : {
                key : 'special_instruction',
                notRequired : true
            },
            'gp-type' : {
                key : 'type',
                notEmpty : {
                    message : 'Gate pass type cannot be empty'
                }
            },  
        }
    });
    
    
    $('#gp-items-form').form_validator({
        events : ['blur'],
        fields : {
            'gp-item-details' : {
                key : 'description',
                notEmpty : {
                    message : 'Item details field cannot be empty'
                }
            },
            'gp-item-unit' : {
                key : 'unit',
                notEmpty : {
                    message : 'Item unit field cannot be empty'
                }
            },
            'gp-item-qty' : {
                key : 'qty',
                notEmpty : {
                    message : 'Item qty field cannot be empty'
                },
                type : {
                    type : 'decimal',
                    message : 'Incorrect Qty'
                }
            },
            'gp-item-status' : {
                key : 'status',
                notEmpty : {
                    message : 'Item status field cannot be empty'
                }
            }
        }
    });
    
    
   TABLE = $('#gete-pass-items').DataTable( {
        "scrollY": "500px",
        "scrollCollapse": true,
        responsive: true,
        /*"processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL+"index.php/user/get_users"
            //"type": "POST"
        },*/
        data : ITEMS,
        "columns": [
            { "data": "line_no" ,
               'width' : '5%' 
               /* "render": function ( data, type, full, meta ){
                    return ''+(meta['row']+1);
                }*/
            },
            { "data": "description" , 'width' : '45%' },
            { "data": "unit" },
            { "data": "qty" },
            { "data": "status" },            
            {
                "orderable": false,
                'data' : 'line_no',
                'width' : '5%', 
                "render": function ( data, type, full, meta ){
                    return '<button class="btn btn-success btn-flat btn-sm" data-index="'+meta['row']+'" onclick="edit_item(this);">Edit</button>';
                }
            },
            {
                "orderable": false,
                'data' : 'line_no',
                'width' : '5%' ,
                render : function(data, type, full, meta){
                    return '<button class="btn btn-danger btn-flat btn-sm" data-index="'+meta['row']+'" onclick="delete_item(this)">Delete</button>';
                }
            }
        ]
    } );
    
    
    var id = $('#gp-id').val();
    if(id != 0)
    {
        load_gate_pass(id);
    }
    /*else
    {
        var curr_date = new Date();
        var curr_date_str = curr_date.getFullYear()+'-';
        (curr_date.getMonth()+1) < 10 ? curr_date_str += '0'+(curr_date.getMonth()+1) : curr_date_str += (curr_date.getMonth()+1);
        curr_date_str += '-';
        curr_date.getDate() < 10 ? curr_date_str += '0'+curr_date.getDate() : curr_date_str += curr_date.getDate();        
        $('#gp-date').val(curr_date_str);
    }*/
    
} );


$('#gp-btn-clear').click(function(){    
    var ans = confirm('Do you want to clear all fields?');
    if(ans == true)
    {
        jsSetFormData([
            {id : 'gp-to' , value : ''},
            {id : 'gp-attention' , value : ''},
            {id : 'gp-remark' , value : ''},
            {id : 'gp-date' , value : ''},
            {id : 'gp-ref' , value : ''},
            {id : 'gp-through' , value : ''},
            {id : 'gp-instructed-by' , value : ''},
            {id : 'gp-special-instruction' , value : ''}
        ]);
        ITEMS = [];
        TABLE.clear().rows.add(ITEMS).draw(false);
    }    
});


$('#gp-btn-new').click(function(){
    // location.reload();
    var ans = confirm('Do you want to move to new gate pass? If you continue all data will be deleted.');
    if(ans == true)
        window.open(BASE_URL+'index.php/gate_pass/new_gate_pass','_self');
});


$('#gp-btn-add').click(function(){
    jsSetFormData([
        {id : 'gp-item-details' , value : ''},
        {id : 'gp-item-unit' , value : ''},
        {id : 'gp-item-qty' , value : ''}
       // {id : 'gp-item-status' , value : ''}
    ]);
    $('#gp-item-details').focus();
    //$('#gp-item-qty-err').html(''); 
    $('#gp-new-item-title').html('New Item');
    $('#gp-btn-add-new-item').show();
    $('#gp-btn-edit-item').hide();
    $('#gp-new-item-model').modal('show');
});


$('#gp-btn-add-new-item').click(function(){
    //var obj = {};
    var obj = $('#gp-items-form').form_validator('validate');
    /*obj['description'] = $('#gp-item-details').val();
    obj['unit'] = $('#gp-item-unit').val();
    obj['qty'] = $('#gp-item-qty').val();
    obj['status'] = $('#gp-item-status').val();*/
    obj['line_no'] = ITEMS.length+1;
    var err_count = 0;
    

    
    //if(err_count == 0)
    if(obj !== false)
    { 
        var trcount = $('#gete-pass-items').find('tbody tr').length;
        /*var node = TABLE.row.add(obj).node();
        var trcount = $('#gete-pass-items').find('tbody tr').length;
		//table.init().data.push(obj);
        console.log(TABLE.init().data);*/
        
        ITEMS.push(obj); 
       //TABLE.clear().rows.add(ITEMS).draw();
        
        var plen = TABLE.page.len() ;
        if(trcount < plen)
            TABLE.clear().rows.add(ITEMS).draw(false);
	else
        {
            //TABLE.draw();
            TABLE.clear().rows.add(ITEMS).draw();
            TABLE.page( 'last' ).draw('page');
            //table.page( 'next' ).draw('page');
	}   
        
       
       jsSetFormData([
        {id : 'gp-item-details' , value : ''},
        {id : 'gp-item-unit' , value : ''},
        {id : 'gp-item-qty' , value : ''},
        {id : 'gp-item-status' , value : 'non returnable'}
    ]);
        //$('#gp-new-item-model').modal('hide');
    }
});


$('#gp-btn-edit-item').click(function(){
   
    var obj = $('#gp-items-form').form_validator('validate');       
    var row = $('#gp-item-arr-index').val();
    
    if(obj !== false)
    {  
        var data = TABLE.row(row ).data();
        obj['line_no'] = data['line_no'];
        ITEMS[row] = obj;
     //  TABLE.row( row ).data(obj).draw();
     TABLE.clear().rows.add(ITEMS).draw(false);
       jsSetFormData([
        {id : 'gp-item-details' , value : ''},
        {id : 'gp-item-unit' , value : ''},
        {id : 'gp-item-qty' , value : ''},
        {id : 'gp-item-status' , value : 'non returnable'}
    ]);
        $('#gp-new-item-model').modal('hide');
    }
});


$('#gp-btn-save').click(function(){
    
    var obj = $('#gp-form').form_validator('validate');
    if(obj !== false)
    {
       var gp_id = $('#gp-id').val();
       
       /*for(var x = 0 ; x < ITEMS.length ; x++)
       {
           delete ITEMS[x]['line_no'];
       }*/
       
       make_ajax_request({
           url : BASE_URL+'index.php/gate_pass/save_gate_pass',
           data : {
               'gp_header' : obj,
               'gp_details' : ITEMS,
               'id' : gp_id
           },
           async : false,
           success : function(response){
               try{
                   var res = JSON.parse(response);
                   if(res['status'] == true)
                   {
                       //alert(res['message']);
                       MSG.data('messageBox').info('Success', res['message']);
                        setTimeout(function(){
                            location.reload();
                        },1000);                       
                   }
                   else
                   {
                       //alert(res['message']);
                       MSG.data('messageBox').danger('Error', res['message']);
                   }
               }
               catch(e)
               {
                   alert(e);
               }
           }
       });
    }
});


$('#gp-btn-approval-send').click(function(){
    
    var ans = confirm('Do you want to send this gate pass for approvel.');
    if(ans == true)
    {
        var id = $('#gp-id').val();
        make_ajax_request({
            url : BASE_URL+'index.php/gate_pass/send_to_approval',
            data : {'id' : id},
            success : function(response){
                try{
                    var obj = JSON.parse(response);
                    $('#gp-btn-approval-send').hide();
                    alert(obj['message']);
                    location.reload();
                }
                catch(e){
                    alert(e);
                }
            }
        });
    }
    
});


function edit_item(ele)
{
    var row_index = $(ele).attr('data-index');
    var data = TABLE.row(row_index ).data();
    $('#gp-item-arr-index').val(row_index);
    if(data != null && data != undefined)
    {
        jsSetFormData([
            {id : 'gp-item-details' , value : data['description']},
            {id : 'gp-item-unit' , value : data['unit']},
            {id : 'gp-item-qty' , value : data['qty']},
            {id : 'gp-item-status' , value : data['status']}
        ]);
    }
    
    $('#gp-new-item-title').html('Edit Item');
    $('#gp-btn-add-new-item').hide();
    $('#gp-btn-edit-item').show();
    $('#gp-new-item-model').modal('show');
}


function delete_item(ele)
{
    var ans = confirm('Do you want to remove this item?');
    if(ans === true)
    {
        var row = $(ele).attr('data-index');
        var trs = $('#gete-pass-items tbody tr');
        var x = 1;
        //TABLE.row(row).remove().draw();
        ITEMS.splice(row,1);
        for(var s = 0 ; s < ITEMS.length ; s++){
            /*var dd = TABLE.row( s ).data();
            dd['line_no'] = s;
            TABLE.row( s ).data( dd ).draw(false);*/
            ITEMS[s]['line_no'] = s+1;
        };
        //TABLE.draw(true);   
        TABLE.clear().rows.add(ITEMS).draw();
    }
}


function load_gate_pass(id)
{
    waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'primary'});
    make_ajax_request({
        url : BASE_URL+'index.php/gate_pass/get_gate_pass',
        data : { id : id},
        success : function(response){
            try{
                var obj = JSON.parse(response);
                if(obj != null && obj != undefined)
                {
                    var ngp_header = obj['ngp_header'];
                    obj['ngp_details'] == null || obj['ngp_details'] == undefined ? ITEMS = [] : ITEMS = obj['ngp_details'];
                    
                    GATE_PASS_HEADER = ngp_header;
                    GATE_PASS_USER = obj['ngp_user'];
                    //alert(response);
                    jsSetFormData([
                        {id : 'gp-no' , value : ngp_header['gp_no']},
                        {id : 'gp-to' , value : ngp_header['gp_to_address']},
                        {id : 'gp-attention' , value : ngp_header['attention']},
                        {id : 'gp-remark' , value : ngp_header['remark']},
                        {id : 'gp-date' , value : ngp_header['date']},
                        {id : 'gp-type' , value : ngp_header['type']},
                        {id : 'gp-ref' , value : ngp_header['style']},
                        {id : 'gp-through' , value : ngp_header['through']},
                        {id : 'gp-instructed-by' , value : ngp_header['instructed_by']},
                        {id : 'gp-special-instruction' , value : ngp_header['special_instruction']}
                    ]);
                    TABLE.clear().rows.add(ITEMS).draw(true);
                                      
                    
                    if(ngp_header['approve'] == 'approval pending' || ngp_header['approve'] == 'approved' || ngp_header['approve'] == 'rejected')
                        $('#gp-btn-approval-send').hide();
                    else
                        $('#gp-btn-approval-send').show();                        
                        
                    $('#gp-status-title').show();
                    
                    var gp_status_btn = $('#gp-status-title-btn');
                    gp_status_btn.removeClass();
                    
                    if(ngp_header['approve'] == 'approval pending')
                        gp_status_btn.addClass('btn btn-info btn-flat btn-sm').html('Approval Pending');                    
                    else if(ngp_header['approve'] == 'approved')
                        gp_status_btn.addClass('btn btn-success btn-flat btn-sm').html('Approved');                        
                    else if(ngp_header['approve'] == 'pending')    
                        gp_status_btn.addClass('btn btn-warning btn-flat btn-sm').html('Pending');
                    else if(ngp_header['approve'] == 'rejected')    
                        gp_status_btn.addClass('btn btn-danger btn-flat btn-sm').html('Rejected');
                    
                    if(ngp_header['approve'] == 'approval pending' || ngp_header['approve'] == 'pending' || ngp_header['approve'] == 'rejected')
                        $('#gp-box').addClass('body-back');
                     
                    if(ngp_header['approve'] == 'approval pending' || ngp_header['approve'] == 'approved' || ngp_header['approve'] == 'rejected')
                        $('#gp-btn-save').hide();
                    
                    setTimeout(function(){
                        waitingDialog.hide();
                    },1000) 
                }               
            }
            catch(e)
            {
                
            }
            
        }
    });
}


function change_gate_pass_status(id,status)
{
//    var can_change = false;
//    if(GATE_PASS_HEADER['type'] === 'style' && USER['user_level'] === 'HOD')
//        can_change = true;
//    else if(GATE_PASS_HEADER['type'] === 'non style' && USER['user_level'] === 'Audit')
//        can_change = true;
//    
//    if(can_change === true)
//    {
        var ans = confirm('Do you want to change gate pass status to '+status+'?');
        if(ans == false)
            return;

        make_ajax_request({
            url : BASE_URL+'index.php/gate_pass/change_gate_pass_status',
            data : {'id' : id , 'status' : status},
            async : false,
            success : function(response){
                try{
                    var obj = JSON.parse(response);
                    alert(obj['message']);
                    if(obj['status'] == true)
                    {
                        location.reload();
                    }
                }
                catch(e){

                }            
            }
        });
    //}  
}


$('#gp-status-title-btn').click(function(){
    var status = $(this).html();
    if(status == 'Approval Pending')
    {
        var can_change = false;
        if(GATE_PASS_HEADER['type'] === 'style' && USER['level_name'] === 'HOD' && GATE_PASS_USER['department'] === USER['department'])
            can_change = true;
        else if(GATE_PASS_HEADER['type'] === 'non style' && USER['user_level'] === 'Audit')
            can_change = true;
        
        if(can_change === true)
            $('#gp-status-change').modal('show');
    }
});


$('#gp-btn-accept').click(function(){
    var id = $('#gp-id').val();
    change_gate_pass_status(id , 'approved');
});


$('#gp-btn-reject').click(function(){
    var id = $('#gp-id').val();
    change_gate_pass_status(id , 'rejected');
});


$('#gp-btn-print').click(function(){
    window.open(BASE_URL+'index.php/gate_pass/print_gate_pass/'+GATE_PASS_HEADER['id']);
});