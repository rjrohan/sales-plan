/*
 * param 
 * ...............
 * options - type = object , details which should pass to the ajax function
 * 
 * return 
 * ..........
 * type = object
 * data which came from server
 */
function make_ajax_request(options)
{
    var response = null;
    var defaults = {
         type : 'post',
         url : '',
         async : true,
         data : {},
         success : function(_response){
             response = JSON.parse(_response);
         },
         error : function(xhr,status,err) {
             console.log(xhr.responseText);
         }
    };
    
    var obj = $.extend({} , defaults , options);    
    $.ajax(obj);
    return response;    
}



function jsSetFormData(arr)
{
    try
    {
        if(arr !== undefined && arr !== null)
        {
            for(var x = 0 , arrLength = arr.length; x < arrLength ; x++)
            {
                var obj = arr[x];					
                if(obj['type'] === undefined || obj['type'] === 'text')
                {
                    $('#'+obj['id']).val(obj['value']);
                }
                else
                {
                }
            }
        }
        return true;
    }
    catch(_exception)
    {
        console.log(_exception);
        return false;
    }
}
