<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {
    
    public function __construct()
    {
         parent::__construct();
         $this->load->model('login_model');
         $status = $this->login_model->user_authentication();
         if($status != true)
             redirect('login');
    }
    
    public function index()
    {
        $data = array();
        $data['menu'] = $this->login_model->get_menu();
        $this->load->view('home/02_home',$data);
    }
    
}