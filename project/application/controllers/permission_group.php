<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permission_Group extends CI_Controller {
    
    public function __construct()
    {
         parent::__construct();
         $this->load->model('login_model');
         $this->load->model('permission_group_model');
         $status = $this->login_model->user_authentication();
         if($status != true)
             redirect ('login');
    }
    
    public function index()
    {
        $data = array();
        $data['menu'] = $this->login_model->get_menu();
        $this->load->view('admin/permissions/permission_groups',$data);
    }
    
    
    public function new_permission_group()
    {
        $data = array();
        $data['menu'] = $this->login_model->get_menu();
        $data['group_id'] = 0;
        $this->load->view('admin/permissions/new_permission_group',$data);
    }
    
    
    public function open_permission_group($id)
    {
        $data = array();
        $data['menu'] = $this->login_model->get_menu();
        $data['group_id'] = $id;
        $this->load->view('admin/permissions/new_permission_group',$data);
    }
    
    
    public function get_permissions_group()
    {
        $id = $this->input->post('group_id');
        $data = array();
        $data['group_details'] = $this->permission_group_model->get_permission_group($id);
        $data['group_permissions'] = $this->permission_group_model->get_group_permissions($id);
        echo json_encode($data);
    }
    
    
    public function get_permissions_groups()
    {
        $data = $_GET;
        $start = $data['start'];
        $length = $data['length'];
        $draw = $data['draw'];
        $search = $data['search']['value'];
        $order = $data['order'][0];       
        $order_column = $data['columns'][$order['column']]['data'].' '.$order['dir'];
        
        $groups = $this->permission_group_model->get_permissions_groups($start,$length,$search,$order_column);
        $count = $this->permission_group_model->get_permissions_groups_count($search);
        echo json_encode(array(
            "draw" => $draw,
            "recordsTotal" => $count,
            "recordsFiltered" => $count,
            "data" => $groups
        ));
    }
    
    
    public function save_permission_group()
    {
        $group_id = $this->input->post('group_id');
        if($group_id == 0)
        {
            $status = $this->permission_group_model->insert_permission_group();
        }
        else{
            $status = $this->permission_group_model->update_permission_group();
        }
        $data = array('status' => $status);
        if($status == true)
            $data['message'] = 'Details saved successfully.';
        else
            $data['message'] = 'Details saving process failed.';
        echo json_encode($data);
        
    }
    
    
    public function get_menu_list()
    {
        $data = $this->permission_group_model->get_all_menu_list();
        echo json_encode($data);
    }
    
    
    public function get_all_permission_groups()
    {
        $data = $this->permission_group_model->get_all_permission_groups();
        echo json_encode($data);
    }
    
    
    public function delete_permission_group($id = 0)
    {
        $status = $this->permission_group_model->delete_permission_group($id);
        $data = array('status' => $status);
        if($status == true)
        {
            $data['message'] = 'Permission group was deleted successfully.';
        }
        else {
            $data['message'] = 'Permission group deleting process failed.';
        }
        echo json_encode($data);
    }
    
    
}