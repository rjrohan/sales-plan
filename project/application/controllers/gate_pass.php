<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gate_Pass extends CI_Controller {
    
    public function __construct()
    {
         parent::__construct();
         $this->load->model('login_model');
         $this->load->model('gate_pass_model');
         $status = $this->login_model->user_authentication();
         if($status != true)
             redirect('login');
    }
    
    public function index()
    {
        $data = array();
        $data['menu'] = $this->login_model->get_menu();
        $this->load->view('gate_pass/gate_pass',$data);
    }
    
    public function new_gate_pass()
    {
        $gp_no = 'GP' . ($this->gate_pass_model->get_max_gate_pass() + 1 + 1000);
        $data['gp_no'] = $gp_no;
        $data['id'] = 0;
        $data['menu'] = $this->login_model->get_menu();
        $data['current_date'] = date("Y-m-d");
        $this->load->view('gate_pass/new_gate_pass',$data);
    }
    
    
    public function open_gate_pass($id = 0)
    {
        $data['gp_no'] = '';
        $data['id'] = $id;
        $data['current_date'] = '';
        $data['menu'] = $this->login_model->get_menu();
        $this->load->view('gate_pass/new_gate_pass',$data);
    }
    
    
    public function save_gate_pass()
    {
        $id = $this->input->post('id');
        $data = array();
        if($id == 0)
        {
            $status = false;
            $gp_id = $this->gate_pass_model->insert_gate_pass();
            $gp_id > 0 ? $status = true : $status = false;
            $data['status'] = $status;
            if($status == true)
                $data['message'] = 'Gate pass was saved successfully.';
            else
                $data['message'] = 'Gate pass was not saved successfully.';    
            $this->gate_pass_email($gp_id);
        }
        else
        {
            $status = $this->gate_pass_model->update_gate_pass();
            $data['status'] = $status;
            if($status == true)
                $data['message'] = 'Gate pass was saved successfully.';
            else
                $data['message'] = 'Gate pass was not saved successfully.'; 
        }        
        echo json_encode($data);
    }
    
    
    public function get_gate_passes()
    {
        $data = $_GET;
        $start = $data['start'];
        $length = $data['length'];
        $draw = $data['draw'];
        $search = $data['search']['value'];
        $order = $data['order'][0];       
        $order_column = $data['columns'][$order['column']]['data'].' '.$order['dir'];
        
        $gp = $this->gate_pass_model->get_gate_passes($start,$length,$search,$order_column);
        $count = $this->gate_pass_model->get_gate_passes_count($search);
        echo json_encode(array(
            "draw" => $draw,
            "recordsTotal" => $count,
            "recordsFiltered" => $count,
            "data" => $gp
        ));
    }
    
    
    public function get_gate_pass()
    {
        $this->load->model('user_model');
        $id = $this->input->post('id');
        $data = array();
        $data['ngp_header'] = $this->gate_pass_model->get_gete_pass($id);
        $data['ngp_details'] = $this->gate_pass_model->get_gate_pass_items($id);
        $data['ngp_user'] = $this->user_model->get_user_from_id($data['ngp_header']['user']);
        echo json_encode($data);
    } 
    
    
    public function delete_gate_pass()
    {
        $id = $this->input->post('id');
        $status = $this->gate_pass_model->delete_gate_pass($id);
        $data = array('status' => $status);
        if($status == true)
            $data['message'] = 'Gate pass was deleted successfully.';
        else
            $data['message'] = 'Gate pass deleting process failed';
        echo json_encode($data);
    }
    
    
    public function send_to_approval()
    {
        $id = $this->input->post('id');
        $status = $this->gate_pass_model->change_gate_pass_status($id,'approval pending');
        $data = array('status' => $status);
        if($status == true)
        {
            $data['message'] = 'Gate pass was send to approval.';
            $this->gate_pass_send_approval_email($id);
        }            
        else
            $data['message'] = 'process error';
        echo json_encode($data);
    }
    
    
    public function change_gate_pass_status()
    {
        $id = $this->input->post('id');
        $gp_status = $this->input->post('status');
        $status = $this->gate_pass_model->change_gate_pass_status($id,$gp_status);
        $data = array('status' => $status);
        if($status == true)
            $data['message'] = 'Status was changed successfully.';
        else
            $data['message'] = 'Process Error';
        echo json_encode($data);
    }
    
    
    public function print_gate_pass($id = 0)
    {
        $this->load->model('user_model');
        $data = array();
        $data['ngp_header'] = $this->gate_pass_model->get_gete_pass($id);
        $data['ngp_details'] = $this->gate_pass_model->get_gate_pass_items($id);
        $data['ngp_authorized_user'] = $this->user_model->get_user_from_id($data['ngp_header']['approved_by']);
        $this->load->view('gate_pass/gate_pass_print',$data);
    }
    
    
    private function get_gate_pass_mail_body_content1($ngp_header)
    {
        return '<table style="width:100%;font-size:12px">
                        <tr>
                            <td style="width:20%"><label>Gate pass no </label></td>
                            <td style="width:30%"> : '.$ngp_header['gp_no'].'</td>
                            <td style="width:20%"><label>Gate pass type </label></td>
                            <td style="width:30%"> : '.$ngp_header['type'].'</td>
                        </tr>
                        <tr>
                            <td><label>Gate pass to </label></td>
                            <td> : '.$ngp_header['gp_to_address'].'</td>
                            <td><label>REF / Style </label></td>
                            <td> : '.$ngp_header['style'].'</td>
                        </tr>
                        <tr>
                            <td><label>Attention </label></td>
                            <td> : '.$ngp_header['attention'].'</td>
                            <td><label>Through </label></td>
                            <td> : '.$ngp_header['through'].'</td>
                        </tr>
                        <tr>
                            <td><label>Remarks </label></td>
                            <td> : '.$ngp_header['remark'].'</td>
                            <td><label>Instructed By </label></td>
                            <td> : '.$ngp_header['instructed_by'].'</td>
                        </tr>
                        <tr>
                            <td><label>Date </label></td>
                            <td> : '.$ngp_header['date'].'</td>
                            <td><label>Special Instruction </label></td>
                            <td> : '.$ngp_header['special_instruction'].'</td>
                        </tr>
                    </table>'; 
    }
    
    
    private function get_gate_pass_mail_body_content2($ngp_details)
    {
        $str = '<table style="width:100%;margin-top: 25px;font-size:12px;border: 1px solid #d9d9d9;" >
                        <thead>
                            <tr>
                                <th style="width:10%;border: 1px solid #d9d9d9;margin-top:2px">No</th>
                                <th style="width:45%;border: 1px solid #d9d9d9;margin-top:2px">Details</th>
                                <th style="width:15%;border: 1px solid #d9d9d9;margin-top:2px">Unit</th>
                                <th style="width:15%;border: 1px solid #d9d9d9;margin-top:2px">Qty</th>
                                <th style="width:15%;border: 1px solid #d9d9d9;margin-top:2px">Status</th>
                            </tr>
                        </thead>
                <tbody>';
                        
        $count1 = 0;
        $count2 = 0;
        foreach ($ngp_details as $row) {
            $str .= '<tr>
            <td style="border: 1px solid #d9d9d9;margin-top:2px">'.$row['line_no'].'</td>
            <td style="border: 1px solid #d9d9d9;margin-top:2px">'.$row['description'].'</td>
            <td style="border: 1px solid #d9d9d9;margin-top:2px">'.$row['unit'].'</td>
            <td style="border: 1px solid #d9d9d9;margin-top:2px">'.$row['qty'].'</td>
            <td style="border: 1px solid #d9d9d9;margin-top:2px">'.$row['status'].'</td>
            </tr>';
            $count1++;
            $count2++;
        }
        $str .= '</tbody> </table>';
        return $str;
    }
    
    
    private function gate_pass_send_approval_email($gp_id)
    {
        $this->load->library('Email_Sender');
        $this->load->model('user_model');
        
        $ngp_header = $this->gate_pass_model->get_gete_pass($gp_id);
        $ngp_details = $this->gate_pass_model->get_gate_pass_items($gp_id);
        if($ngp_header['type'] == 'style')
            $ngp_authorized_user = $this->user_model->get_user_hod($ngp_header['user']);
        else
            $ngp_authorized_user = $this->user_model->get_user_audit();
        
        if($ngp_authorized_user == null || $ngp_authorized_user == false)
            return false;
        
        $mail_arr = array();
        
        $data = array();             
        $data['header1'] = 'GENERAL GATE PASS';
        $data['header2'] = '';
        $data['header_text'] = 'Gate Pass Approval Pending';  
        $data['content1'] = $this->get_gate_pass_mail_body_content1($ngp_header);         
        $data['content2'] = $this->get_gate_pass_mail_body_content2($ngp_details);      
        
        $arr = array(
            'to' => $ngp_authorized_user['email'],
            'subject' => 'General Gate Pass ('.$ngp_header['gp_no'].')',
            'html_data' => $data,
            'attachments' => null
        );
        array_push($mail_arr,$arr);
        return $this->email_sender->send_mail($mail_arr);
    }
    
    
    private function gate_pass_email($gp_id)
    {
        $this->load->library('Email_Sender');
        $this->load->model('user_model');
        
        $ngp_header = $this->gate_pass_model->get_gete_pass($gp_id);
        $ngp_details = $this->gate_pass_model->get_gate_pass_items($gp_id);        
        $ngp_user = $this->user_model->get_user_from_id($ngp_header['user']);      
        
        if($ngp_user == null || $ngp_user == false)
            return false;
        
        $mail_arr = array();        
        $data = array();             
        $data['header1'] = 'GENERAL GATE PASS';
        $data['header2'] = '';
        $data['header_text'] = 'Gate Pass Approval Pending';  
        $data['content1'] = $this->get_gate_pass_mail_body_content1($ngp_header);         
        $data['content2'] = $this->get_gate_pass_mail_body_content2($ngp_details); 
        
        $pdfFilePath = FCPATH."assets/documents/gate_pass_".$gp_id.".pdf";
        $data['page_title'] = 'Hello world'; // pass data to the view
        
        if (file_exists($pdfFilePath) == TRUE)
        {           
           unlink($pdfFilePath); 
        }

        if (file_exists($pdfFilePath) == FALSE)
        {
            $data_pdf = array(
                'ngp_header' => $ngp_header,
                'ngp_details' => $ngp_details,
                'ngp_authorized_user' => $this->user_model->get_user_from_id($ngp_header['approved_by'])
            );
            ini_set('memory_limit','32M'); // boost the memory limit if it's low <img class="emoji" draggable="false" alt="😉" src="https://s.w.org/images/core/emoji/72x72/1f609.png">
            $html = $this->load->view('gate_pass/gate_pass_print',$data_pdf,true);            
            $this->load->library('pdf');
            $pdf = $this->pdf->load();
            $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img class="emoji" draggable="false" alt="😉" src="https://s.w.org/images/core/emoji/72x72/1f609.png">
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'F'); // save to file because we can
            //$content = $mpdf->Output('', 'S');
        }        
       
        $arr = array(
            'to' => $ngp_user['email'],
            'subject' => 'General Gate Pass ('.$ngp_header['gp_no'].')',
            'html_data' => $data,
            'attachments' => array($pdfFilePath)
        );
        array_push($mail_arr,$arr);
        return $this->email_sender->send_mail($mail_arr);
    }
    
    
}