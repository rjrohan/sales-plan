<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Status_Report extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('work_center_model');
        $this->load->model('buyers_model');
        $this->load->model('status_report_model');
        $status = $this->login_model->user_authentication();
        if($status != true)
            redirect('login');
    }
    
    
    public function index()
    {
        $data = array();
        $data['menu'] = $this->login_model->get_menu();
        $this->load->view('status_report/status_report',$data);
    }
    
    
    public function get_status_report()
    {
        $start_date_str = $this->input->post('start');
        $start_date = date_create($this->input->post('start'));
        $end_date = date_create($this->input->post('end'));
        $report = array();
        
        $buyers = $this->buyers_model->get_all_buyers();
        $work_centers = $this->work_center_model->get_all_work_centers();
        
        $details = array();
        $tttt = 0;
        foreach($work_centers as $wc)
        {
            //$status_arr = array('factory' => $wc['description'] );
            $status_arr = array('factory' => $wc['description'] , 'factory_code'=>$wc['work_center_no'], 'data' => array());
           // $arr1 = array();
            $start_date = date_create($start_date_str);
            for($i = $start_date; $i <= $end_date; $i->modify('+1 day')){ 
                $arr1 = array('date' => null , 'status' => array());
                $arr1['date'] = $i->format('Y-m-d');
                $arr2 = array();
                foreach($buyers as $b)
                {
                    $grn_count = 0;//$this->status_report_model->get_grn_count($b['buyer_code'],$i->format('d/m/Y'),$wc['work_center_no']);
                    /*$r = rand(5, 15);
                    if($r%2 == 0)
                        $r = 0;
                    else 
                        $r = 1;*/
                   $arr2[$b['buyer_code']] = $grn_count;//'17'; 
                   //break;
                }
                $arr1['status'] = $arr2;           
                //echo $i->format("Y-m-d");
                array_push($status_arr['data'], $arr1);
                //break;
            }
           // $status_arr['data'] = $arr1;
            array_push($details,$status_arr);
           
        }
        
        
        
        $report['buyers'] = $buyers;
        $report['report'] = $details;
        echo json_encode($report);
    }
    
    
    public function get_grn_count()
    {
        $buyer = $this->input->post('buyer');
        $date = date_create($this->input->post('date'));
        $factory = $this->input->post('factory');
        $data = array('grn_count' => 0 , 'pcd_count' => 0);
        
        $pcd_count = $this->status_report_model->get_pcd_avaliable($buyer,$date->format('d/m/Y'),$factory);
        $data['pcd_count'] = $pcd_count;
        if($pcd_count > 0)
        {
            //$data['grn_count'] = $this->status_report_model->get_grn_count($buyer,$date->format('d/m/Y'),$factory);
            $data['grn_count'] = $this->status_report_model->get_pending_grn_count($buyer,$date->format('d/m/Y'),$factory); 
        }        
        echo json_encode($data);
    }
    
    
    public function grn_pending($buyer = "" , $date = "" , $factory_code = "")
    {
        $data = array();
        $data['menu'] = $this->login_model->get_menu();
        $data['buyer'] = $buyer;
        $data['date'] = $date;
        $data['factory_code'] = $factory_code;
        $this->load->view('status_report/grn_pending',$data);
    }
    
    
    public function get_pending_grn()
    {
        $buyer = $this->input->get('buyer');
        $date = date_create($this->input->get('date'));
        $factory_code = $this->input->get('factory_code');    
        $data = $_GET;
        $start = $data['start'];
        $length = $data['length'];
        $draw = $data['draw'];
       // $search = $data['search']['value'];
        $order = $data['order'][0];       
        $order_column = $data['columns'][$order['column']]['data'].' '.$order['dir'];
        
        $list = $this->status_report_model-> get_pending_grn($buyer,$date->format('d/m/Y'),$factory_code,$start,$length,$order_column);
        $count = $this->status_report_model->get_pending_grn_count($buyer,$date->format('d/m/Y'),$factory_code);
        echo json_encode(array(
            "draw" => $draw,
            "recordsTotal" => $count,
            "recordsFiltered" => $count,
            "data" => $list
        ));
    }
    
    
}