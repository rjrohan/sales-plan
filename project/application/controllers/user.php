<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
    
    public function __construct()
    {
         parent::__construct();
         $this->load->model('user_model');
         $this->load->model('user_level_model');
         $this->load->model('login_model');
         
         $status = $this->login_model->user_authentication();
         if($status != true)
             redirect ('login');
         // Your own constructor code
    }
    
    public function index()
    {
        $data = array();
        $data['menu'] = $this->login_model->get_menu();
        $this->load->view('admin/users/06_users',$data);
    }
    
    public function new_user()
    {
        $this->load->model('permission_group_model');
        $user_levels = $this->user_level_model->get_all_user_levels();
        $departments = $this->user_model->get_departments();
        $data = array('user_levels' => $user_levels , 'departments' => $departments);
        $data['id'] = 0;
        $data['menu'] = $this->login_model->get_menu();
        $data['permission_groups'] = $this->permission_group_model->get_all_permission_groups();
        $this->load->view('admin/users/05_new_user',$data);
    }
    
    
    public function open_user($id = 0)
    {
        $this->load->model('permission_group_model');
        $user_levels = $this->user_level_model->get_all_user_levels();
        $departments = $this->user_model->get_departments();
        $data = array('user_levels' => $user_levels , 'departments' => $departments);
        $data['menu'] = $this->login_model->get_menu();
        $data['id'] = $id;
        $data['permission_groups'] = $this->permission_group_model->get_all_permission_groups();
        $this->load->view('admin/users/05_new_user',$data);  
    }
    
    
    public function get_user()
    {
       $id = $this->input->post('id');
       $user = $this->user_model->get_user_from_id($id);
       echo json_encode($user);
    }
    
    
    public function get_users()
    {
        $data = $_GET;
        $start = $data['start'];
        $length = $data['length'];
        $draw = $data['draw'];
        $search = $data['search']['value'];
        $order = $data['order'][0];       
        $order_column = $data['columns'][$order['column']]['data'].' '.$order['dir'];
        
        $users = $this->user_model->get_users($start,$length,$search,$order_column);
        $count = $this->user_model->get_users_count($search);
        echo json_encode(array(
            "draw" => $draw,
            "recordsTotal" => $count,
            "recordsFiltered" => $count,
            "data" => $users
        ));
    }
    
    
    public function is_username_exists()
    {
        $username = $this->input->post('value');
        $id = $this->input->post('id');
        $user = $this->user_model->get_user_from_username($username);        
        $data = array();
        if($user == null || $user == false)
            $data['status'] = true;
        else
        {
            if($id == $user['id'])
                $data['status'] = true;
            else
            {
                $data['status'] = false;
                $data['message'] = 'Username already exists in the system.';
            }            
        }            
        echo json_encode($data);
    }
    
    
    public function is_email_exists()
    {
        $email = $this->input->post('value');
        $id = $this->input->post('id');
        $user = $this->user_model->get_user_from_email($email);        
        $data = array();
        if($user == null || $user == false)
            $data['status'] = true;
        else
        {
            if($id == $user['id'])
                $data['status'] = true;
            else
            {
                $data['status'] = false;
                $data['message'] = 'Email already exists in the system.';
            }            
        }            
        echo json_encode($data);
    }
    
    
    public function save_user()
    {
       // $save_status = $this->input->post('save_status');
        $id = $this->input->post('id');
        $dep = $this->input->post('department');
        $user_level = $this->input->post('user_level');
        $data = array();
        
        $validate = $this->user_model->validate_user_level($id,$dep,$user_level);
        if($validate == false)
        {
            $data['status'] = false;
            $data['message'] = 'Selected department already have a HOD.';
        }
        else 
        {
            if($id == 0)
            { 
                $status = $this->user_model->insert_new_user();                              
            }
            else //if($save_status == 'edit')
            {
                $status = $this->user_model->update_user();                 
            }
            
            if($status == false)
                $data['message'] = 'Details saving process was failed.';
            else
                $data['message'] = 'Details were saved successfully.';
            $data['status'] = $status;
        }
        echo json_encode($data);
    }
    
    
    public function delete_user($id = 0)
    {
        $status = $this->user_model->delete_user($id);
        $data = array('status' => $status);
        if($status == true)
        {
            $data['message'] = 'User was deleted successfully.';
        }
        else {
            $data['message'] = 'User deleting process failed.';
        }
        echo json_encode($data);
    }
    
    
    public function user_account()
    {
        $id = $this->session->userdata('user_id');
        if($id > 0)
        {
            $user_levels = $this->user_level_model->get_all_user_levels();
            $departments = $this->user_model->get_departments();
            $data = array('user_levels' => $user_levels , 'departments' => $departments);
            $data['menu'] = $this->login_model->get_menu();
            $data['id'] = $id;
            $this->load->view('admin/users/user_account',$data);
        }
        else
        {
            redirect('login');
        }
    }
    
    
    
}