<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
    
    public function __construct()
    {
         parent::__construct();
         $this->load->model('login_model');
         $this->load->helper('form');
    }
    
    public function index()
    {
        $data = array();
        //$data['menu'] = $this->login_model->get_menu();
        $data['err_message'] = '';
        $this->load->view('login/01_login',$data);
    }
    
    public function user_login()
    {       
        $this->load->library('form_validation');
    
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        
        if($this->form_validation->run() == FALSE)
        {
            $data = array('err_message' => 'Username and password cannot be empty.');
            $this->load->view('login/01_login',$data);
        }
        else
        {
            $email = $this->input->post('username');
            $password = $this->input->post('password');
            
            $arr = explode('@', $email);
            if(sizeof($arr) <= 1)
               $email = $email.'@jinadasa.com';
       
           /// $user_details = $this->login_model->find_user_by_username($username,$password);
            $user_details = $this->login_model->find_user_by_email($email,$password);
            $size = sizeof($user_details);
            $data = array('login_status' => false);
            if($size > 0)
            {           
                $this->session->set_userdata('username',$user_details['user_name']);
                $this->session->set_userdata('user_id',$user_details['id']);
                $this->session->set_userdata('user_level',$user_details['user_level']);
                $this->session->set_userdata('level_name',$user_details['level_name']);
                $this->session->set_userdata('department',$user_details['department']);
                $this->session->set_userdata('dep_name',$user_details['dep_name']);
                //$this->session->userdata($user_details);
                //$this->session->set_userdata('username',$user_details['user_name']);
                //$this->session->set_userdata('password','');
                $this->session->set_userdata('loged_in',true);
                
                $permission_group = $user_details['permission_group'];
                $this->load->model('permission_group_model');
                $this->session->set_userdata('permissions',  $this->permission_group_model->get_group_permissions($permission_group));
                redirect('main');
            }
            else {
                $data = array('err_message' => 'Incorrect Email or password');
                $this->load->view('login/01_login',$data);
            }               
        }
    }
    
    
    public function user_logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }
    
    
    public function forgot_password()
    {
        $this->load->view('login/forgot_password');
    }
    
    public function reset_password()
    {
        $this->load->view('login/reset_password');
    }
}

