<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales_Plan extends CI_Controller {
    
    public function __construct()
    {
         parent::__construct();
         $this->load->model('login_model');
         $this->load->model('sales_plan_model');
         
         $status = $this->login_model->user_authentication();
         if($status != true)
             redirect ('login');
    }
    
    public function sales_plan_view()
    {
        $data = array();
        $data['menu'] = $this->login_model->get_menu();
        $this->load->view('sales_plan/03_sales_plan_view',$data);
    }
    
    
    public function sales_plan_edit()
    {
        $data = array();
        $data['menu'] = $this->login_model->get_menu();
        $this->load->view('sales_plan/04_sales_plan_edit',$data);
    }
    
    
    public function get_buyers()
    {
        $data = $this->sales_plan_model->get_buyers();
        echo json_encode($data);
    }
    
    public function get_sales_plan()
    {
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        
        $start    = new DateTime($from_date.'-01');
        $start->modify('first day of this month');
        //echo $start;
        $end      = new DateTime($to_date.'-01');
        $end->modify('first day of next month');
        //echo $end;
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start, $interval, $end);
        $days = array();
        foreach ($period as $dt) {
            array_push($days,$dt->format("Y-m"));
        }        
        
        $buyers = $this->sales_plan_model->get_buyers();
        /*$buyers = array(
            array('REGION_CODE' => 'CK MEN','DESCRIPTION' => 'CK MEN'),
            array('REGION_CODE' => 'CK WAMEN','DESCRIPTION' => 'CK WAMEN'),
            array('REGION_CODE' => 'WARNER','DESCRIPTION' => 'WARNER')
        );*/
        
        $plan = array();
        foreach($buyers as $buyer)
        {
            $qty_arr = array();
            foreach($days as $day)
            {
                $saved_plan = $this->sales_plan_model->get_sales_plan($buyer['REGION_CODE'],$day);
                if($saved_plan == null || $saved_plan == false)
                {
                    array_push($qty_arr , array(
                        'month' => $day,
                        //'qty' => $this->sales_plan_model->get_customer_order_qty($buyer['REGION_CODE'],$day) 
                        'qty_confirmed' => $this->sales_plan_model->get_customer_order_qty($buyer['REGION_CODE'],$day),
                        'qty_projection' => '',
                        'qty_target' => '',
                        'qty_previous' => ''
                    ));
                }
                else {
                   array_push($qty_arr , array(
                        'month' => $day,
                        //'qty' => $this->sales_plan_model->get_customer_order_qty($buyer['REGION_CODE'],$day) 
                        'qty_confirmed' => $this->sales_plan_model->get_customer_order_qty($buyer['REGION_CODE'],$day),
                        'qty_projection' => $saved_plan['qty_projection'],
                        'qty_target' => $saved_plan['qty_target'],
                        'qty_previous' => $saved_plan['qty_previous']
                    ));
                }
                
            }
            array_push($plan,array('buyer' => $buyer['REGION_CODE'],'buyer_description' => $buyer['DESCRIPTION'] , 'details' => $qty_arr));
        }        
        echo json_encode($plan);
    }
    
    
    public function save_sales_plan()
    {
        $plan = $this->input->post('plan');
        $months = $this->input->post('months');
        foreach ($months as $month)
        {
            $this->sales_plan_model->delete_sales_plan($month);
        }       
        $this->sales_plan_model->insert_sales_plan($plan);
    }
    
}
    