<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FDN | Add/Edit Gate Pass</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <!-- bootstrap datepicker -->
     <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
    
    <!-- datatables -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/datatables.css">
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/messagebox/messagebox.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
   
    <style>
        .body-back {
    background-image: url("<?php echo base_url();?>assets/img/wm3.jpg");
    background-color: #ffffff;
    background-repeat: no-repeat;
    background-size: 700px 500px;
    background-position: 200px 50px
}
    </style>

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

      <!-- main header -->
      <?php $this->load->view('common/header'); ?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <?php $this->load->view('common/left_menu'); ?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper ">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            General Gate Pass
            <!--<small>it all starts here</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>index.php/main"><i class="fa fa-dashboard"></i> Home</a></li>        
            <li class="active">New Gate Pass</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><!--Title--></h3>
              <div class="box-tools pull-right">
                <!--<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>-->
                  <div style="margin-right: 30px;display: none" id="gp-status-title">
                    <label>STATUS : </label>  
                    <button class="btn btn-info btn-flat btn-sm" id="gp-status-title-btn" style="width:120px">PENDING</button>  
                 </div>
              </div>
            </div>
              <div class="box-body" id="gp-box">
        
                <div class="col-lg-12" id="gp-form">
                    
                    <input type="hidden" id="gp-id" value="<?php echo $id; ?>">
                    
                    <div class="col-lg-6">
                        <!--<div class="form-group">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 25%;padding-right: 15px"><input class="form-control" type="text" ></td>
                                    <td style="width: 60%"><input class="form-control" type="text"></td>
                                    <td style="padding-left: 15px"><button class="btn btn-primary btn-flat">Search</button></td>
                                </tr>
                            </table>
                        </div>-->
                        <div class="form-group">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 25%"><label>Gate pass no</label></td>
                                    <td style="width: 63%"><input class="form-control" type="text" id="gp-no" disabled="disabled" value="<?php echo $gp_no; ?>"></td>
                                    <td style="padding-left: 15px"><button class="btn btn-primary btn-flat" id="gp-btn-new">New</button></td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 25%"><label>Gate pass to</label></td>
                                    <td><input class="form-control" type="text" id="gp-to"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 25%"><label>Attention</label></td>
                                    <td><input class="form-control" type="text" id="gp-attention"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group">
                            <table style="width: 100%">
                                <tr >
                                    <td style="width: 25%"><label>Remarks</label></td>
                                    <td><input class="form-control" type="text" id="gp-remark"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group">
                            <table style="width: 100%">
                                <tr >
                                    <td style="width: 25%"><label>Date</label></td>
                                    <td><input class="form-control" type="text" id="gp-date" disabled="disbled" value="<?php echo $current_date; ?>"></td>
                                </tr>
                            </table>
                        </div>                  
                    </div> 
                    <div class="col-lg-6">
                        
                        <div class="form-group">
                            <table style="width: 100%">
                                <tr >
                                    <td style="width: 25%"><label>Gate Pass Type</label></td>
                                    <td>
                                        <select class="form-control" type="text" id="gp-type">
                                            <option value="style">Style</option>
                                            <option value="non style">Non Style</option>
                                        </select>                                  
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group">
                            <table style="width: 100%">
                                <tr >
                                    <td style="width: 25%"><label>REF / Style</label></td>
                                    <td><input class="form-control" type="text" id="gp-ref"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group">
                            <table style="width: 100%">
                                <tr >
                                    <td style="width: 25%"><label>Through</label></td>
                                    <td><input class="form-control" type="text" id="gp-through"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group">
                            <table style="width: 100%">
                                <tr >
                                    <td style="width: 25%"><label>Instructed By</label></td>
                                    <td><input class="form-control" type="text" id="gp-instructed-by"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group">
                            <table style="width: 100%">
                                <tr >
                                    <td style="width: 25%"><label>Special Instruction</label></td>
                                    <td><input class="form-control" type="text" id="gp-special-instruction"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-12" style="text-align: left">                        
                        <button class="btn btn-primary btn-flat" id="gp-btn-add">Add</button>
                    </div>
                </div>   
                
                <div class="col-lg-12" style="margin-top: 20px">
                    <table id="gete-pass-items" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Details</th>
                                <th>Unit</th>
                                <th>Qty</th>
                                <th>Status</th>
                                <th>Edit</th>
                                <th>Delete</th>                            
                            </tr>
                        </thead>
                        <tfoot>

                        </tfoot>
                    </table> 
                </div>
                
                <div class="col-lg-12" style="text-align: right;margin-top: 25px">
                    <button class="btn btn-primary btn-flat" id="gp-btn-save">Save</button>
                    <button class="btn btn-primary btn-flat" id="gp-btn-approval-send" style="display: none">Send to Approval</button>
                    <button class="btn btn-danger btn-flat" id="gp-btn-clear">Clear All</button>
                    <button class="btn btn-info btn-flat" id="gp-btn-print">Print</button>
                </div>
        
            </div><!-- /.box-body -->
            <div class="box-footer" style="text-align: right">
              
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

     
      <!-- footer -->
      <?php $this->load->view('common/footer'); ?>

      <!-- Control Sidebar -->
      <?php //$this->load->view('common/control_bar'); ?>
      
      <!-- /.control-sidebar -->
      
      
    </div><!-- ./wrapper -->
    
    
    
    
    <!-- Modal -->
<div id="gp-new-item-model" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="gp-new-item-title">  </h4>
      </div>
      <div class="modal-body">
        
          <div id="gp-items-form">
              
              <input type="hidden" value="-1" id="gp-item-arr-index">
              
              <div class="form-group">
                  <label>Details</label>
                  <input type="text" class="form-control" id="gp-item-details">
              </div>
              <div class="form-group">
                  <label>Unit</label>
                  <input type="text" class="form-control" id="gp-item-unit">
              </div>
              <div class="form-group">
                  <label>Qty</label>
                  <input type="text" class="form-control" id="gp-item-qty">
              </div>
              <div class="form-group">
                  <label>Status</label>
                  <select class="form-control" id="gp-item-status">
                      <option value="returnable">Returnable</option>
                      <option value="non returnable" selected="selected">Non Returnable</option>
                  </select>
              </div>
          </div> 
          
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="gp-btn-add-new-item">Add</button>  
          <button type="button" class="btn btn-primary" id="gp-btn-edit-item" style="display: none">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    
    
    
    
    
<!-- Modal -->
<div id="gp-status-change" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Gate pass status change</h4>
      </div>
      <div class="modal-body">
        
          <button type="button" class="btn btn-success"  style="width:95%" id="gp-btn-accept">Accept</button><br><br>  
          <button type="button" class="btn btn-danger" style="width:95%" id="gp-btn-reject">Reject</button>
          
      </div>
      <div class="modal-footer">
          
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    
    
    

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- bootstrap calendar -->
    <script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
    <!-- page js file -->
    <script src="<?php echo base_url(); ?>assets/views/gate_pass/new_gate_pass.js"></script>
    <script src="<?php echo base_url(); ?>assets/application/app.js"></script>
    
    <!-- jquery form validator plugin -->
    <script src="<?php echo base_url(); ?>/assets/application/form_validator_v2.js"></script>
    
    <script  src="<?php echo base_url(); ?>assets/application/waiting_dialogbox.js"></script>
    
    <!-- datatables -->
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/datatables.js"></script>
    
    <script  src="<?php echo base_url(); ?>assets/plugins/messagebox/jquery.messagebox.min.js"></script>
    
    <script>
        var BASE_URL = '<?php echo base_url(); ?>';
        var USER = {
            'username' : '<?php echo $this->session->userdata('username'); ?>',
            'user_id' : '<?php echo $this->session->userdata('user_id'); ?>',
            'user_level' : '<?php echo $this->session->userdata('user_level'); ?>',
            'level_name' : '<?php echo $this->session->userdata('level_name'); ?>',
            'department' : '<?php echo $this->session->userdata('department'); ?>',
            'dep_name' : '<?php echo $this->session->userdata('dep_name'); ?>'
        }
    </script>   
    
  </body>
</html>
