<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FDN | Print Gate Pass</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.css">   
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

    <style>
     /*  #gpp-items tbody tr:nth-of-type(even) {
  background-color: #ebebe0;  
}*/

#gpp-items tr {
    height: 35px
}

#gpp-items td {
    border: 1px solid #e0e0d1;
    text-align: center;   
}

#gpp-items th {
    border: 1px solid #e0e0d1;
    text-align: center;   
}

.body-back {
    
    background-color: #ffffff;
    background-repeat: repeat-y;
    background-size: 700px 1000px
}

    </style>

    </head>
    
    <?php if($ngp_header['approve'] == 'approved') { ?>
    <body style="display: none">
    <?php }else { ?>         
    <body  style="background-image: url('<?php echo base_url();?>assets/img/wm2.jpg'); background-color: #ffffff;background-repeat: repeat-y;background-size: 700px 1000px">
    <?php } ?>
        <div class="container">      
            <div class="row">
                <div class="col-lg-12">
                    
                    <div >                        
                        <table style="margin-top: 10px" id="tst">
                            <tr>
                                <td><img src="<?php echo base_url(); ?>assets/img/logo2.png" style="margin-left: -10px"></td>
                                <td>
                                    <div style="text-align: center;line-height: 80%;">
                                        <h2>Foundation Garments (Pvt) Ltd</h2>
                                        <p>VAT NO: 114042005-7000 (Compnay Reg. No: PV2646)</p>
                                        <p>No: 106/4, Dutugemunu Street, Kohuwala, Sri Lanka</p>
                                        <p>Tel:+94-11-4385400, Fax: +94-11-2769088</p>
                                        <p>E-mail: accjho@jinadasa.com Web : www.fdnsl.com</p> 
                                    </div>
                                </td>
                            </tr>
                        </table>                 
                    </div>
                    <div style="text-align: center;">
                        <hr>
                        <h4><b>GENERAL GATE PASS</b></h4>
                        <hr>
                    </div>
                    
                    <table style="width:100%;margin-top: 20px;">
                        <tr>
                            <td style="width:20%"><label>Gate pass no </label></td>
                            <td style="width:30%"><?php echo $ngp_header['gp_no']; ?></td>
                            <td style="width:20%"><label>Gate pass type </label></td>
                            <td style="width:30%"><?php echo $ngp_header['type']; ?></td>
                        </tr>
                        <tr>
                            <td><label>Gate pass to </label></td>
                            <td><?php echo $ngp_header['gp_to_address']; ?></td>
                            <td><label>REF / Style </label></td>
                            <td><?php echo $ngp_header['style']; ?></td>
                        </tr>
                        <tr>
                            <td><label>Attention </label></td>
                            <td><?php echo $ngp_header['attention']; ?></td>
                            <td><label>Through </label></td>
                            <td><?php echo $ngp_header['through']; ?></td>
                        </tr>
                        <tr>
                            <td><label>Remarks </label></td>
                            <td><?php echo $ngp_header['remark']; ?></td>
                            <td><label>Instructed By </label></td>
                            <td><?php echo $ngp_header['instructed_by']; ?></td>
                        </tr>
                        <tr>
                            <td><label>Date </label></td>
                            <td><?php echo $ngp_header['date']; ?></td>
                            <td><label>Special Instruction </label></td>
                            <td><?php echo $ngp_header['special_instruction']; ?></td>
                        </tr>
                    </table>
                    
                    <table style="width:100%;margin-top: 25px" id="gpp-items">
                        <thead>
                            <tr style="background-color: none;">
                                <th>No</th>
                                <th>Details</th>
                                <th>Unit</th>
                                <th>Qty</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $count1 = 0;
                            $count2 = 0;
                            foreach ($ngp_details as $row) {
                              /*  if($count1 == 16)
                                {
                                   echo '<tr style="height:10px"></tr>';
                                   $count2 = 0;
                                }                                    
                                else if($count2 == 26)
                                {
                                   echo '<tr style="height:12px;"></tr>'; 
                                   $count2 = 0;
                                }
                                  */  
                            ?>
                            <tr>
                                <td><?php echo $row['line_no']; ?></td>
                                <td><?php echo $row['description']; ?></td>
                                <td><?php echo $row['unit']; ?></td>
                                <td><?php echo $row['qty']; ?></td>
                                <td><?php echo $row['status']; ?></td>
                            </tr>
                            <?php 
                            $count1++;
                            $count2++;
                            } ?>
                        </tbody>
                        
                    </table>
                    
                    <div style="margin-top: 45px">
                        <div style="float: left;text-align: center">
                            <?php if(sizeof($ngp_authorized_user) > 0) {?>
                                <span><?php echo $ngp_authorized_user['first_name'].' '.$ngp_authorized_user['last_name'].' ('.$ngp_authorized_user['dep_name'].')'; ?></span>                        
                            <?php } ?>
                                <br><span>................................................................</span>
                            <br>
                            <label>Authorized By</label>
                            
                        </div> 
                        <div style="float: right;text-align: center" >
                            <br>
                            <span>.........................................................</span><br>
                            <label >Security Check</label>
                            
                        </div>
                    </div>                           
                </div>          
            </div>
        </div>   
      
     

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>

    <script>
        
        $(document).ready(function(){
            window.print();
            /*setTimeout(function(){
                $('body').hide();
            },1000)*/
           // $('body').hide();
        });
        
       /* $('body').hide();
        printWindow.document.getElementById('showTopDetailsContent').style.display='block';
        printWindow.document.write(printContent.innerHTML);*/
        
    </script>
    
  </body>
</html>
