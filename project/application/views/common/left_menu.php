<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url(); ?>assets/dist/img/avatar6.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $this->session->userdata('username'); ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
       
            <?php 
            
                if($menu != null && $menu != false)
                {
                    foreach($menu as $row)
                    {           
                        echo '<li class="treeview">';
                        echo '<a href="'.base_url().$row['level1']['menu_link'].'">';
                        echo '<i class="'.$row['level1']['icon'].'"></i>';
                        echo '<span>'.$row['level1']['menu_text'].'</span>';
                        echo '</a>';
                        if($row['level2'] != null && $row['level2'] != false)   
                        {
                            echo '<ul class="treeview-menu">';
                            foreach ($row['level2'] as $row2)
                            {
                                echo '<li><a href="'.base_url().$row2['menu_link'].'"><i class="'.$row2['icon'].'"></i>'.$row2['menu_text'].'</a></li>';
                            }
                            echo '</ul>';
                        }                             
                        echo '</li>';            
                    }
                }            
            ?>
            
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>