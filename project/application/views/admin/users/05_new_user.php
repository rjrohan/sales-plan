<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FDN | Add/Edit User</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <!-- bootstrap datepicker -->
     <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/messagebox/messagebox.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

      <!-- main header -->
      <?php $this->load->view('common/header'); ?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <?php $this->load->view('common/left_menu'); ?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 id="05-title">
            New User
            <!--<small>it all starts here</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>index.php/main"><i class="fa fa-dashboard"></i> Home</a></li>            
            <li class="active">New User</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"> <!--Title--></h3>
<!--              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>-->
            </div>
            <div class="box-body">
        
                <div class="col-lg-12" id="05-form">
                    
                    <input type="hidden" value="<?php echo $id; ?>" id="05-user-id">
                    
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" placeholder="Enter first name" id="05-first-name">
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" placeholder="Enter last name" id="05-last-name">
                        </div>
                        <div class="form-group">
                            <label>Contact No</label>
                            <input type="text" maxlength="10" class="form-control" placeholder="Enter phone number" id="05-contact-no">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" placeholder="Enter email address" id="05-email">
                        </div>
                        <div class="form-group">
                            <label>Permission Group</label>
                            <Select class="form-control" id="05-permission-group">
                                <?php foreach ($permission_groups as $row) { ?>
                                    <option value="<?php echo $row['group_id']; ?>"><?php echo $row['group_name']; ?></option>
                                <?php } ?>                                
                            </select>
                        </div> 
                    </div>
                    
                    <div class="col-lg-6">
                        <!--<div class="form-group">
                            <label>Username</label>
                            <input type="text" class="form-control" placeholder="Enter username" id="05-username">
                        </div>-->
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" placeholder="Enter passsword" id="05-password">
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" class="form-control" placeholder="Confirm your password" id="05-conf-password">
                        </div>
                        <div class="form-group">
                            <label>Department</label>
                            <select class="form-control" id="05-department">   
                                <?php foreach ($departments as $row) { ?>
                                    <option value="<?php echo $row['dep_id']; ?>"><?php echo $row['dep_name']; ?></option>
                                <?php } ?>  
                            </select>
                        </div>
                        <div class="form-group">
                            <label>User Level</label>
                            <Select class="form-control" id="05-user-level">
                                <?php foreach ($user_levels as $row) { ?>
                                    <option value="<?php echo $row['level_id']; ?>"><?php echo $row['level_name']; ?></option>
                                <?php } ?>                                
                            </select>
                        </div                       
                    </div>             
                    
                </div>   
                
        
            </div><!-- /.box-body -->
            <div class="box-footer" style="text-align: right">
                <button class="btn btn-flat btn-primary" style="margin-right: 30px" id="05-btn-save">Save Details</button>
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

     
      <!-- footer -->
      <?php $this->load->view('common/footer'); ?>

      <!-- Control Sidebar -->
      <?php //$this->load->view('common/control_bar'); ?>
      
      <!-- /.control-sidebar -->
      

      
   
   
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>    
    <!-- SlimScroll -->
    <script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>    
    <!-- page js file -->
    <script src="<?php echo base_url(); ?>assets/views/admin/users/05_new_user.js"></script>
    <script src="<?php echo base_url(); ?>assets/application/app.js"></script>
    <!-- jquery form validator plugin -->
    <script src="<?php echo base_url(); ?>/assets/application/form_validator.js"></script>
    <!-- waiting popup -->
    <script  src="<?php echo base_url(); ?>assets/application/waiting_dialogbox.js"></script>
    
    <script  src="<?php echo base_url(); ?>assets/plugins/messagebox/jquery.messagebox.min.js"></script>
    
    <script>
        var BASE_URL = '<?php echo base_url(); ?>';
    </script>
    
    
    
  </body>
</html>
