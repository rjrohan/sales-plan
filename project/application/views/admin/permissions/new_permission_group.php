<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FDN | Add/Edit Permissions</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <!-- bootstrap datepicker -->
     <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/messagebox/messagebox.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

      <!-- main header -->
      <?php $this->load->view('common/header'); ?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <?php $this->load->view('common/left_menu'); ?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            NEW PERMISSION GROUP
            <!--<small>it all starts here</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>index.php/main"><i class="fa fa-dashboard"></i> Home</a></li>            
            <li class="active">New Permission Group</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><!--Title--></h3>
<!--              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>-->
            </div>
            <div class="box-body">
        
                <div class="col-lg-12">
                    <div class="col-lg-6" id="pg-form">
                        <div class="form-group">
                            <label>Permission Group Name</label> 
                            <input type="text" class="form-control" placeholder="Enter group name" id="pg-group-name">                       
                        </div>
                        
                        <button class="btn btn-primary btn-flat" style="margin-top: 20px" id="pg-btn-save">Save Changes</button>
                    </div>
                    <div class="col-lg-6" style="overflow-y: scroll;max-height: 300px">
                        <table class="table" id="pg-permission-list">
                            <thead>
                                <tr>
                                    <th style="width: 50px"></th>
                                    <th>Permission</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>                        
                    </div>
                </div>
          
        
            </div><!-- /.box-body -->
            <div class="box-footer" style="text-align: right">
              
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

     
      <!-- footer -->
      <?php $this->load->view('common/footer'); ?>

      <!-- Control Sidebar -->
      <?php //$this->load->view('common/control_bar'); ?>
      
      <!-- /.control-sidebar -->
   
   
   
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- bootstrap calendar -->
    <script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
    <!-- page js file -->
    <script src="<?php echo base_url(); ?>assets/views/admin/permissions/new_permission_group.js"></script>
    <script src="<?php echo base_url(); ?>assets/application/app.js"></script>

    <!-- jquery form validator plugin -->
    <script src="<?php echo base_url(); ?>/assets/application/form_validator.js"></script>
    <script  src="<?php echo base_url(); ?>assets/application/waiting_dialogbox.js"></script>
    
     <script  src="<?php echo base_url(); ?>assets/plugins/messagebox/jquery.messagebox.min.js"></script>
    
    <script>
        
        var PAGE = (function(){
            var _base_url = '<?php echo base_url(); ?>';
            var _group_id = '<?php echo $group_id; ?>';
            var _msg = null;
            return {
                getBaseUrl : function(){ return _base_url;},
                getGroupId : function(){ return _group_id;},
                setGroupId : function(_id) {_group_id = _id;},
                getMessageBox : function(){return _msg;},
                setMessageBox : function(_mb) {_msg = _mb;},
            };
        })();
    </script>
    
    
    
  </body>
</html>
