<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FDN | Sales Plan Edit</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <!-- bootstrap datepicker -->
     <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>
        .currency {
            text-align: right;           
        } 
        
      /*  #04-sales-plan-table td {
            width:200px;
        }*/
        
   /*  .table-wrapper { 
    overflow-x:scroll;
    overflow-y:visible;
    width:1000px;
}*/


    /*td:nth-child(1n+4){
    //padding: 5px 20px;
    background-color: #0073b7;
    width: 50px;
    }*/
        
        .title {
            text-align: center;            
        }
        
        .table-input {
            width : 100%;
            height : 100%;
            padding-left: -10px;
            padding-right: -10px;
            padding-bottom: -10px;
            padding-top: -10px
        }
       /* tr:nth-child(even) {background: #CCC}
        tr:nth-child(odd) {background: #FFF}*/
        /*td:nth-child(1) {background: #B5D1D8}*/
    </style>
    
    
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

      <!-- main header -->
      <?php $this->load->view('common/header'); ?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <?php $this->load->view('common/left_menu'); ?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Edit Sales Plan
            <!--<small>it all starts here</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>index.php/main"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Edit Sales Plan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Title</h3>
<!--              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>-->
            </div>
            <div class="box-body">
        
                <table>
                    <tr>
                        <td>
                            <div class="input-group input-daterange" id="datepicker" style="margin-left: 30px">
                                <span class="input-group-addon">From</span>
                                <input type="text" class="form-control" value="" id="04-from-date">
                                <span class="input-group-addon" style="padding-left: 25px">to</span>
                                <input type="text" class="form-control" value="" id="04-to-date">
                            </div>
                        </td>
                        <td>
                            <button class="btn btn-primary btn-flat" style="margin-left: 15px" id="04-btn-search">Search</button> 
                        </td>
                    </tr>
                </table>        
        
                <div style="overflow: scroll;width: 100%">
                    <table class="table table-bordered" style="margin-top: 20px;" id="04-sales-plan-table">
                        <thead >
                        <!--<tr>
                            <td>Year</td>
                            <td></td>
                            <td></td>
                            <td>2015-11-11</td>
                            </tr>-->
                        </thead>
                        <tbody>
                            <!--<tr>
                                <td>Buyer</td>
                                <td>Target</td>
                                <td>Confirmed</td>
                                <td>Projection</td>
                            </tr>-->
                        </tbody>                   
                    </table>
                </div>
                
                 
                     
          
        
            </div><!-- /.box-body -->
            <div class="box-footer" style="text-align: right">
               <!-- <button class="btn btn-flat btn-success" onclick="$('#04-sales-plan-table').tableExport({type:'excel',escape:'false','htmlContent':'true'});">Export to excel</button>-->
                <button class="btn btn-primary btn-flat" id="04-btn-save">Save Plan</button>
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

     
      <!-- footer -->
      <?php $this->load->view('common/footer'); ?>

      <!-- Control Sidebar -->
      <?php //$this->load->view('common/control_bar'); ?>
      
      <!-- /.control-sidebar -->
      
      
      
      <div id="c04-loader-model" class="fade modal" data-backdrop="static">
  
   <div class="modal-dialog" style="width:10%">
  
  	<div style="text-align: center">
  		<label style="color: #fff;font-size: 15px">Loading.....</label>
  		<img alt="" src="<c:url value="/resources/img/loading.gif" />">
  	</div>
  </div>
</div><!-- /.modal --> 
      
   
   
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- bootstrap calendar -->
    <script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
    <!-- page js file -->
    <script src="<?php echo base_url(); ?>assets/views/sales_plan/04_sales_plan_edit.js"></script>
    <script src="<?php echo base_url(); ?>assets/application/app.js"></script>
    <!-- document exporter -->
    <script src="<?php echo base_url(); ?>assets/plugins/document_exporter/tableExport.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/document_exporter/jquery.base64.js"></script>
    
    <script  src="<?php echo base_url(); ?>assets/application/waiting_dialogbox.js"></script>
    
    <script>
        var BASE_URL = '<?php echo base_url(); ?>';
    </script>
    
    
    
  </body>
</html>
