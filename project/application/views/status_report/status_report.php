<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FDN | Status Report</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <!-- bootstrap datepicker -->
     <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <style>
        
        #status-report-table th {
            min-width : 70px;
            text-align: center;            
        }
        
        #status-report-table td {
            height : 50px;
            text-align: center;
            vertical-align : middle
        }
        
        .report-table { /*border-collapse:separate; border-top: 3px solid grey; */}
    .report-table td ,th {
        margin:0;
        /*border:3px solid grey;*/
        /*border-top-width:0px;*/
        /*white-space:nowrap;*/
        height:40px;
        border: 1px solid #d9d9d9 !important;
    }
    .report-div {
        /*width: 92%;
        overflow-x:scroll;  
        margin-left:5em;
        overflow-y:visible;
        padding-bottom:1px;*/
        overflow-x:scroll;
        margin-left:135px;
        padding-bottom:1px
    }
    .headcol {
        position:absolute;
        width:140px;
        left:0;
        top:auto;
        border-right: 0px none black;
        /*border-top-width:3px; *//*only relevant for first row*/
        /*margin-top:-3px;*/ /*compensate for top border*/
     
    }
        
    </style>
    
    
    
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

      <!-- main header -->
      <?php $this->load->view('common/header'); ?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <?php $this->load->view('common/left_menu'); ?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Status Report
            <!--<small>it all starts here</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>index.php/main"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Status Report</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><!--Title--></h3>
<!--              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>-->
            </div>
            <div class="box-body">
        
            <div class="report-div">
                <table id="status-report-table" class="table table-bordered report-table" >
                    <thead>
                        
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>            
            </div>
        
            </div><!-- /.box-body -->
            <div class="box-footer" style="text-align: right">
              
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

     
      <!-- footer -->
      <?php $this->load->view('common/footer'); ?>

      <!-- Control Sidebar -->
      <?php //$this->load->view('common/control_bar'); ?>
      
      <!-- /.control-sidebar -->
      
      
   
      
   
   
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    
    <!-- SlimScroll -->
    <script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
     <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
    <!-- page js file -->
    <script src="<?php echo base_url(); ?>assets/views/status_report/status_report.js"></script>
    <script src="<?php echo base_url(); ?>assets/application/app.js"></script>
    
    
    <script  src="<?php echo base_url(); ?>assets/application/waiting_dialogbox.js"></script>
    
    <script>
        var BASE_URL = '<?php echo base_url(); ?>';
    </script>
    
    
    
  </body>
</html>
