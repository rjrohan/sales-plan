<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Login_Model extends CI_Model 
{ 
    private $DB1 = null;
    private $DB2 = null;
    
    public function __construct() 
    { 
        parent::__construct();  
        //$this->DB1 = $this->load->database('default',true);
        $this->DB2 = $this->load->database('second',true);
    } 
    
    public function user_authentication()
    {
        $username = $this->session->userdata('username');
        $logedin = $this->session->userdata('loged_in');
        $status = false;
        if($username != null && $username != false)
        {
            if($logedin == true)
                $status = true;
        }
        return $status;
    }
    
    public function find_user_by_username($username = '',$password = '')
    {
      /* $this->DB2->select('*');
       $this->DB2->from('user');
       $this->DB2->where('user_name',$username);
       $this->DB2->where('password',MD5($password));
       $query = $this->DB2->get();
       return $query->row_array();*/
       $query = $this->DB2->query("SELECT a.*,b.level_name,c.dep_name FROM user a "
                . "LEFT JOIN user_levels b ON a.user_level=b.level_id "
                . "LEFT JOIN departments c ON a.department=c.dep_id WHERE a.user_name='".$username."' AND a.password=MD5('".$password."')");
       return $query->row_array();
    }
    
    
    public function find_user_by_email($email = '',$password = '')
    {
        $query = $this->DB2->query("SELECT a.*,b.level_name,c.dep_name FROM user a "
                . "LEFT JOIN user_levels b ON a.user_level=b.level_id "
                . "LEFT JOIN departments c ON a.department=c.dep_id WHERE a.email='".$email."' AND a.password=MD5('".$password."')");
       return $query->row_array();
    }
    
    
    public function get_menu()
    {
        $menu_arr = array();
        $permissions = $this->session->userdata('permissions');
        if($permissions != null && $permissions != false)
        {
            $query = $this->DB2->query('SELECT * FROM menu_list WHERE menu_level = 1');
            $main_menus = $query->result_array();        
            foreach ($main_menus as $row) {
                $arr1 = array();
                
                $per_arr = array();
		foreach($permissions as $row11)		
                    array_push($per_arr,$row11['menu_id']);		
                //array_column function only works for php version >= 5.5
               // $key = array_search($row['menu_id'], array_column($permissions, 'menu_id'));
                $key = array_search($row['menu_id'], $per_arr);
                if($key !== false && $key !== null)
                {
                    $status = $permissions[$key]['permission_status'];
                    if($status == 1)
                    {
                        $arr1['level1'] = $row; 
                        $query1 = $this->DB2->query("SELECT * FROM menu_list WHERE menu_level = 2 AND menu_parent=".$row['menu_id']);
                        $arr1['level2'] = $query1->result_array();
                        array_push($menu_arr, $arr1);
                    } 
                }       
            }  
        }        
        return $menu_arr;
    }
    
}