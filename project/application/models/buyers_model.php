<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Buyers_Model extends CI_Model 
{ 
    private $DB1 = null;
    private $DB2 = null;
    
    public function __construct() 
    { 
        parent::__construct();  
        //$this->DB1 = $this->load->database('default',true);
        $this->DB2 = $this->load->database('second',true);
    } 
    
    
    public function get_all_buyers()
    {
        $this->DB2->select('*');
        $this->DB2->from('buyer');
        $query = $this->DB2->get();
        return $query->result_array();
    }
    
    
}