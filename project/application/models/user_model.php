<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class User_Model extends CI_Model 
{ 
    private $DB1 = null;
    private $DB2 = null;
    
    public function __construct() 
    { 
        parent::__construct();  
        //$this->DB1 = $this->load->database('default',true);
        $this->DB2 = $this->load->database('second',true);
    } 
    
    
    public function get_user_from_username($username)
    {
        $query = $this->DB2->query("SELECT a.*,b.level_name,c.dep_name FROM user a "
                . "LEFT JOIN user_levels b ON a.user_level=b.level_id "
                . "LEFT JOIN departments c ON a.department=c.dep_id WHERE a.user_name='".$username."'");
        
       /* $this->DB2->select('*');
        $this->DB2->from('user');
        $this->DB2->where('user_name',$username);
        $query = $this->DB2->get();*/
        return $query->row_array();
    }
    
    
    public function get_user_from_email($email)
    {
        $query = $this->DB2->query("SELECT a.*,b.level_name,c.dep_name FROM user a "
                . "LEFT JOIN user_levels b ON a.user_level=b.level_id "
                . "LEFT JOIN departments c ON a.department=c.dep_id WHERE a.email='".$email."'");
        
       /* $this->DB2->select('*');
        $this->DB2->from('user');
        $this->DB2->where('user_name',$username);
        $query = $this->DB2->get();*/
        return $query->row_array();
    }
    
    
    public function get_user_from_id($id)
    {
        /*$this->DB2->select('*');
        $this->DB2->from('user');
        $this->DB2->where('id',$id);
        $query = $this->DB2->get();*/
        $query = $this->DB2->query("SELECT a.*,b.level_name,c.dep_name FROM user a "
                . "LEFT JOIN user_levels b ON a.user_level=b.level_id "
                . "LEFT JOIN departments c ON a.department=c.dep_id WHERE a.id='".$id."'");
        return $query->row_array();
    }
    
    
    public function get_users($start,$limit,$search,$order)
    {
        $query = $this->DB2->query("SELECT a.*,b.level_name FROM user a,user_levels b WHERE a.user_level=b.level_id AND (a.id LIKE '%".$search."%' OR a.user_name LIKE '%".$search."%' OR b.level_name LIKE '%".$search."%' "
                . "OR a.first_name LIKE '%".$search."%' OR a.last_name LIKE '%".$search."%' OR a.contact_no LIKE '%".$search."%' OR a.email LIKE '%".$search."%') "
                . "ORDER BY ".$order." LIMIT ".$start.",".$limit);
        return $query->result_array();
    }
    
    
    public function get_users_count($search)
    {
        $query = $this->DB2->query("SELECT COUNT(a.id) as row_count FROM user a,user_levels b WHERE a.user_level=b.level_id AND (a.user_name LIKE '%".$search."%' OR b.level_name LIKE '%".$search."%' "
                . "OR a.first_name LIKE '%".$search."%' OR a.last_name LIKE '%".$search."%' OR a.contact_no LIKE '%".$search."%' OR a.email LIKE '%".$search."%')");
        $result = $query->row_array();
        return $result['row_count'];
        
    }
    
    
    public function insert_new_user()
    {
        try{
            $data = array(
                'user_name' => $this->input->post('user_name'),
                'password' => md5($this->input->post('password')),
                'user_level' => $this->input->post('user_level'),
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'contact_no' => $this->input->post('contact_no'),
                'email' => $this->input->post('email'),
                'status' => 'active',
                'department' => $this->input->post('department'),
                'permission_group' => $this->input->post('permission_group')
            );
            $this->DB2->insert('user',$data);   
            return true;
        } catch (Exception $ex) {
            return false;
        }        
    }
    
    
    public function update_user()
    {
        try{
            $data = array(
                'user_name' => $this->input->post('user_name'),                
                'user_level' => $this->input->post('user_level'),
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'contact_no' => $this->input->post('contact_no'),
                'email' => $this->input->post('email'),
                'status' => 'active',
                'department' => $this->input->post('department'),
                'permission_group' => $this->input->post('permission_group')
            );
            if($this->input->post('password') != '')
                $data['password'] = md5($this->input->post('password'));
            
            $this->DB2->where('id',  $this->input->post('id'));
            $this->DB2->update('user',$data);   
            return true;
        } catch (Exception $ex) {
            return false;
        }        
    }
    
    
    public function delete_user($id)
    {
       try{
           $this->DB2->delete('user',array('id' => $id));
           return true;
       } catch (Exception $ex) {
           return false;
       }
    }
    
    
    public function get_departments()
    {
        $this->DB2->select('*');
        $this->DB2->from('departments');
        $query = $this->DB2->get();
        return $query->result_array();
    }
    
    
    public function validate_user_level($id , $dep , $level)
    {
        if($level == 2)
        {
            if($id == 0)
            {
                $query = $this->DB2->query("SELECT id FROM user WHERE department=".$dep." AND user_level =".$level);
                if($query->num_rows() > 0)
                    return false;
                else
                    return true;
            }            
            else {
                $query = $this->DB2->query("SELECT id FROM user WHERE department=".$dep." AND user_level =".$level);
                if($query->num_rows() > 0)
                {
                    $arr = $query->row_array();
                    if($id == $arr['id'])
                        return true;
                    else
                        return false;                    
                }
                else {
                    return true;
                }
            }
        }
        else
            return true;
    }
    
    
    public function get_user_hod($user_id)
    {
        $query = $this->DB2->query("SELECT u.* FROM user u,departments d,user_levels ul WHERE d.dep_id = (SELECT department FROM user WHERE id=".$user_id.") "
                . "AND u.department = d.dep_id AND ul.level_name = 'HOD' AND ul.level_id = u.user_level");
        return $query->row_array();
    }
    
    
    public function get_user_audit()
    {
        $query = $this->DB2->query("SELECT u.* FROM user u,user_levels ul WHERE ul.level_name='Audit' AND ul.level_id=u.user_level");
        return $query->row_array();        
    }
    
    
} 