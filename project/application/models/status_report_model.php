<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Status_Report_Model extends CI_Model 
{ 
    private $DB1 = null;
    private $DB2 = null;
    
    public function __construct() 
    { 
        parent::__construct();         
        $this->DB1 = $this->load->database('default',true);
        $this->DB2 = $this->load->database('second',true);
    } 
    
    
    public function get_grn_count($buyer,$date,$factory)
    {
        /*$query = $this->DB1->query("SELECT \"COUNT\"(IFSAPP.DOP_SUPPLY_PURCH_ORD.ORDER_NO) AS grn_count FROM IFSAPP.DOP_SUPPLY_PURCH_ORD
            INNER JOIN IFSAPP.DOP_DEMAND_CUST_ORD ON IFSAPP.DOP_SUPPLY_PURCH_ORD.DOP_ID = IFSAPP.DOP_DEMAND_CUST_ORD.DOP_ID
            INNER JOIN IFSAPP.CUSTOMER_ORDER_LINE ON IFSAPP.DOP_DEMAND_CUST_ORD.ORDER_NO = IFSAPP.CUSTOMER_ORDER_LINE.ORDER_NO
            AND IFSAPP.DOP_DEMAND_CUST_ORD.LINE_NO = IFSAPP.CUSTOMER_ORDER_LINE.LINE_NO
            AND IFSAPP.DOP_DEMAND_CUST_ORD.REL_NO = IFSAPP.CUSTOMER_ORDER_LINE.REL_NO
            INNER JOIN IFSAPP.PURCHASE_ORDER ON IFSAPP.DOP_SUPPLY_PURCH_ORD.ORDER_NO = IFSAPP.PURCHASE_ORDER.ORDER_NO
            INNER JOIN IFSAPP.DOP_SUPPLY_SHOP_ORD ON IFSAPP.DOP_SUPPLY_PURCH_ORD.DOP_ID = IFSAPP.DOP_SUPPLY_SHOP_ORD.DOP_ID
            INNER JOIN IFSAPP.SHOP_ORDER_OPERATION ON IFSAPP.DOP_SUPPLY_SHOP_ORD.ORDER_NO = IFSAPP.SHOP_ORDER_OPERATION.ORDER_NO
            AND IFSAPP.DOP_SUPPLY_SHOP_ORD.RELEASE_NO = IFSAPP.SHOP_ORDER_OPERATION.RELEASE_NO
            AND IFSAPP.DOP_SUPPLY_SHOP_ORD.SEQUENCE_NO = IFSAPP.SHOP_ORDER_OPERATION.SEQUENCE_NO
            WHERE                     
            (
                NVL(
                    (SELECT SUM(T .qty_arrived) FROM PURCHASE_RECEIPT_NEW T WHERE T .order_no = IFSAPP.DOP_SUPPLY_PURCH_ORD.ORDER_NO
                        AND T .release_no = IFSAPP.DOP_SUPPLY_PURCH_ORD.RELEASE_NO AND T .line_no = IFSAPP.DOP_SUPPLY_PURCH_ORD.RELEASE_NO ),0)
            )<= 0               
            AND IFSAPP.SHOP_ORDER_OPERATION.WORK_CENTER_NO = '".$factory."'  AND 
            TO_DATE(
                            IFSAPP.CUSTOMER_ORDER_LINE.C_PCD
            )BETWEEN TO_DATE('".$date."', 'DD/MM/YYYY')
            AND TO_DATE('".$date."', 'DD/MM/YYYY')
            AND INVENTORY_PART_API.GET_PART_PRODUCT_FAMILY(
                            IFSAPP.CUSTOMER_ORDER_LINE.CONTRACT,
                            IFSAPP.CUSTOMER_ORDER_LINE.PART_NO
            )= '".$buyer."'");*/
        $query = $this->DB1->query("");
        $res = $query->row_array();
        return $res['GRN_COUNT'];
    }
    
    
    public function get_pending_grn($buyer,$date,$factory_code,$start,$length,$order)
    {
        $query = $this->DB1->query("SELECT * FROM (SELECT rownum rn ,COL.ORDER_NO AS CUSTOMER_ORDER_NO,COL.buy_qty_due AS CUST_ORD_QTY,COL.LINE_NO,COL.REL_NO,COL.LINE_ITEM_NO,COL.CONTRACT,
                COL.STATE,COL.OBJSTATE,COL.C_PCD,COL.PART_NO,
                INVENTORY_PART_API.GET_PART_PRODUCT_FAMILY(COL.CONTRACT, COL.PART_NO)AS PROD_FAMILY,
                COMMODITY_GROUP_API.GET_DESCRIPTION(INVENTORY_PART_API.GET_PRIME_COMMODITY(COL.CONTRACT, SMA.PART_NO)) AS FNG_COLOR,
                INVENTORY_PART_API.GET_DIM_QUALITY(COL.CONTRACT, COL.PART_NO) AS STYLE,
                CUSTOMER_ORDER_API.GET_CUSTOMER_PO_NO(COL.ORDER_NO)AS BUYER_PO,
                INVENTORY_PART_API.GET_PART_PRODUCT_FAMILY(COL.CONTRACT, COL.PART_NO) AS BUYER_CODE,
                DDC.DOP_ID,
                DSSO.ORDER_NO AS SHOP_ORD_NO,
                SOO.WORK_CENTER_NO,                
                SMA.PART_NO AS ITEM_NO,
                INVENTORY_PART_API.GET_DESCRIPTION(COL.CONTRACT, SMA.PART_NO)AS ITEM_DESC,                
                SMA.QTY_ASSIGNED,
                SMA.QTY_ISSUED,
                SMA.QTY_PER_ASSEMBLY,
                SMA.SHRINKAGE_FACTOR,
                SMA.QTY_REQUIRED,
                SMA.STATE AS SHOP_STATE,
                                                                                                                                NVL(DSPO.ORDER_NO, 'No Po') AS PO_NO,
                PURCHASE_ORDER_API.GET_PURCHASE_CODE(DSPO.ORDER_NO)AS MERCHAND,
                COL.DELIVERY_COUNTRY_CODE AS COUNTRY_CODE,
                SMA.DOP_ORDER_ID,
                SMA.RELEASE_NO AS SHO_REL_NO,
                SMA.LINE_ITEM_NO AS SHOP_LINE_NO,
                SMA.SEQUENCE_NO AS SHOP_SEQ,
                SUPPLIER_API.GET_VENDOR_NAME(PURCHASE_ORDER_API.GET_VENDOR_NO(DSPO.ORDER_NO)) AS VENDOR,
                DSPO.RELEASE_NO AS PO_RELEASE_NO,
                DSPO.LINE_NO AS PO_LINE_nO,
                INVENTORY_PART_API.GET_RM_TYPE_ID(COL.CONTRACT, SMA.PART_NO) AS RM_TYPE,
                INVENTORY_PART_API.Get_Unit_Meas(COL.CONTRACT, SMA.PART_NO) AS UOM,
                SHOP_ORD_API.Get_Revised_Qty_Due(order_no_ =>SMA.ORDER_NO ,release_no_ =>SMA.RELEASE_NO ,sequence_no_ =>SMA.SEQUENCE_NO ) AS LOT_SIZE,
                INVENTORY_PART_API.Get_Type_Designation(COL.CONTRACT, COL.PART_NO) LOT
                FROM
                                IFSAPP.CUSTOMER_ORDER_LINE COL
                INNER JOIN IFSAPP.DOP_DEMAND_CUST_ORD DDC ON COL.ORDER_NO = DDC.ORDER_NO
                AND COL.LINE_NO = DDC.LINE_NO
                AND COL.REL_NO = DDC.REL_NO
                AND COL.LINE_ITEM_NO = DDC.LINE_ITEM_NO
                INNER JOIN IFSAPP.DOP_SUPPLY_SHOP_ORD DSSO ON DDC.DOP_ID = DSSO.DOP_ID
                INNER JOIN IFSAPP.SHOP_ORD SO ON DSSO.ORDER_NO = SO.ORDER_NO
                AND DSSO.RELEASE_NO = SO.RELEASE_NO
                AND DSSO.SEQUENCE_NO = SO.SEQUENCE_NO
                INNER JOIN IFSAPP.SHOP_ORDER_OPERATION SOO ON SO.ORDER_NO = SOO.ORDER_NO
                AND SO.RELEASE_NO = SOO.RELEASE_NO
                AND SO.SEQUENCE_NO = SOO.SEQUENCE_NO
                INNER JOIN IFSAPP.SHOP_MATERIAL_ALLOC SMA ON SO.ORDER_NO = SMA.ORDER_NO
                AND SO.RELEASE_NO = SMA.RELEASE_NO
                AND SO.SEQUENCE_NO = SMA.SEQUENCE_NO
                LEFT JOIN IFSAPP.DOP_SUPPLY_PURCH_ORD DSPO ON DDC.DOP_ID = DSPO.DOP_ID
                AND SMA.DOP_ORDER_ID = DSPO.DOP_ORDER_ID 
                INNER JOIN IFSAPP.DOP_ORDER ON IFSAPP.DOP_ORDER.DOP_ID = DSSO.DOP_ID AND IFSAPP.DOP_ORDER.DOP_ORDER_ID = DSSO.DOP_ORDER_ID
                WHERE
                TO_DATE(COL.C_PCD) BETWEEN TO_DATE('".$date."','DD/MM/YYYY') AND  TO_DATE('".$date."','DD/MM/YYYY')
                AND
                SOO.WORK_CENTER_NO = '".$factory_code."'  AND
                INVENTORY_PART_API.GET_PART_PRODUCT_FAMILY(COL.CONTRACT, COL.PART_NO) ='".$buyer."'                     
                AND (
                    NVL( ( SELECT SUM(T .qty_arrived) FROM PURCHASE_RECEIPT_NEW T WHERE
                        T .order_no = DSPO.ORDER_NO
                        AND T .release_no = DSPO.RELEASE_NO
                        AND T .line_no = DSPO.LINE_NO
                    ), 0)
                )<= 0 AND IFSAPP.DOP_ORDER.REVISED_QTY_DUE > 0 ORDER BY ".$order." 
                ) WHERE rn >= ".($start+1)." and rn <= ".($start+$length));
            return $query->result_array();
    }
    
    
    
    public function get_pending_grn_count($buyer,$date,$factory_code)
    {
        $query = $this->DB1->query("SELECT COUNT(COL.ORDER_NO) AS ROW_COUNT 
                FROM 
                IFSAPP.CUSTOMER_ORDER_LINE COL
                INNER JOIN IFSAPP.DOP_DEMAND_CUST_ORD DDC ON COL.ORDER_NO = DDC.ORDER_NO
                AND COL.LINE_NO = DDC.LINE_NO
                AND COL.REL_NO = DDC.REL_NO
                AND COL.LINE_ITEM_NO = DDC.LINE_ITEM_NO
                INNER JOIN IFSAPP.DOP_SUPPLY_SHOP_ORD DSSO ON DDC.DOP_ID = DSSO.DOP_ID
                INNER JOIN IFSAPP.SHOP_ORD SO ON DSSO.ORDER_NO = SO.ORDER_NO
                AND DSSO.RELEASE_NO = SO.RELEASE_NO
                AND DSSO.SEQUENCE_NO = SO.SEQUENCE_NO
                INNER JOIN IFSAPP.SHOP_ORDER_OPERATION SOO ON SO.ORDER_NO = SOO.ORDER_NO
                AND SO.RELEASE_NO = SOO.RELEASE_NO
                AND SO.SEQUENCE_NO = SOO.SEQUENCE_NO
                INNER JOIN IFSAPP.SHOP_MATERIAL_ALLOC SMA ON SO.ORDER_NO = SMA.ORDER_NO
                AND SO.RELEASE_NO = SMA.RELEASE_NO
                AND SO.SEQUENCE_NO = SMA.SEQUENCE_NO
                LEFT JOIN IFSAPP.DOP_SUPPLY_PURCH_ORD DSPO ON DDC.DOP_ID = DSPO.DOP_ID
                AND SMA.DOP_ORDER_ID = DSPO.DOP_ORDER_ID 
                INNER JOIN IFSAPP.DOP_ORDER ON IFSAPP.DOP_ORDER.DOP_ID = DSSO.DOP_ID AND IFSAPP.DOP_ORDER.DOP_ORDER_ID = DSSO.DOP_ORDER_ID
                WHERE
                TO_DATE(COL.C_PCD) BETWEEN TO_DATE('".$date."','DD/MM/YYYY') AND  TO_DATE('".$date."','DD/MM/YYYY')
                AND
                SOO.WORK_CENTER_NO = '".$factory_code."'  AND
                INVENTORY_PART_API.GET_PART_PRODUCT_FAMILY(COL.CONTRACT, COL.PART_NO ) ='".$buyer."' AND (
                    NVL( ( SELECT SUM(T .qty_arrived) FROM PURCHASE_RECEIPT_NEW T WHERE
                        T .order_no = DSPO.ORDER_NO
                        AND T .release_no = DSPO.RELEASE_NO
                        AND T .line_no = DSPO.LINE_NO
                    ), 0)
                )<= 0 AND IFSAPP.DOP_ORDER.REVISED_QTY_DUE > 0");
        $res = $query->row_array();
        return $res['ROW_COUNT'];
    }
    
    
    public function get_pcd_avaliable($buyer,$date,$factory)
    {
        $query = $this->DB1->query("SELECT
            COUNT(IFSAPP.CUSTOMER_ORDER_LINE.ORDER_NO) AS ORDER_COUNT 
            FROM 
            IFSAPP.DOP_DEMAND_CUST_ORD
            INNER JOIN IFSAPP.CUSTOMER_ORDER_LINE ON IFSAPP.DOP_DEMAND_CUST_ORD.ORDER_NO = IFSAPP.CUSTOMER_ORDER_LINE.ORDER_NO
            AND IFSAPP.DOP_DEMAND_CUST_ORD.LINE_NO = IFSAPP.CUSTOMER_ORDER_LINE.LINE_NO
            AND IFSAPP.DOP_DEMAND_CUST_ORD.REL_NO = IFSAPP.CUSTOMER_ORDER_LINE.REL_NO
            INNER JOIN IFSAPP.DOP_SUPPLY_SHOP_ORD ON IFSAPP.DOP_SUPPLY_SHOP_ORD.DOP_ID = IFSAPP.DOP_DEMAND_CUST_ORD.DOP_ID
            INNER JOIN IFSAPP.SHOP_ORDER_OPERATION ON IFSAPP.DOP_SUPPLY_SHOP_ORD.ORDER_NO = IFSAPP.SHOP_ORDER_OPERATION.ORDER_NO
            AND IFSAPP.DOP_SUPPLY_SHOP_ORD.RELEASE_NO = IFSAPP.SHOP_ORDER_OPERATION.RELEASE_NO
            AND IFSAPP.DOP_SUPPLY_SHOP_ORD.SEQUENCE_NO = IFSAPP.SHOP_ORDER_OPERATION.SEQUENCE_NO
            WHERE
            IFSAPP.SHOP_ORDER_OPERATION.WORK_CENTER_NO = '".$factory."'
            AND TO_DATE(
                IFSAPP.CUSTOMER_ORDER_LINE.C_PCD
            )BETWEEN TO_DATE('".$date."', 'DD/MM/YYYY')
            AND TO_DATE('".$date."', 'DD/MM/YYYY')
            AND INVENTORY_PART_API.GET_PART_PRODUCT_FAMILY(
                IFSAPP.CUSTOMER_ORDER_LINE.CONTRACT,
                IFSAPP.CUSTOMER_ORDER_LINE.PART_NO
            )= '".$buyer."'
        ");
        $res = $query->row_array();
        return $res['ORDER_COUNT'];
    }
    
    public function get_status_report()
    {
        
        
    }
    
}