<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Permission_Group_Model extends CI_Model 
{ 
    private $DB1 = null;
    private $DB2 = null;
    
    public function __construct() 
    { 
        parent::__construct();  
        //$this->DB1 = $this->load->database('default',true);
        $this->DB2 = $this->load->database('second',true);
    }
    
    
    public function get_all_permission_groups()
    {
        $this->DB2->select('*');
        $this->DB2->from('permission_groups');
        $query = $this->DB2->get();
        return $query->result_array();
    }
    
    
    public function get_all_menu_list()
    {
        $this->DB2->select('*');
        $this->DB2->from('menu_list');
        $query = $this->DB2->get();
        return $query->result_array();
    }
    
    
    public function insert_permission_group()
    {
        try{
            $arr = array(
                'group_name' => $this->input->post('group_name')
            );
            $this->DB2->insert('permission_groups',$arr);
            $id = $this->DB2->insert_id();
            if($id > 0)
            {
                $permissions = $this->input->post('permissions');

                for($x = 0 ; $x < sizeof($permissions) ; $x++)
                {
                    $permissions[$x]['group_id'] = $id;
                }
                $this->DB2->insert_batch('group_permissions',$permissions);
                return true;
            }
            return false;
        } catch (Exception $ex) {
            return false;
        }        
    }
    
    
    public function update_permission_group()
    {
        try {
            
            $permissions = $this->input->post('permissions');
            $id = $this->input->post('group_id'); 
            $arr = array(
                'group_id' => $id,
                'group_name' => $this->input->post('group_name')
            );
            $this->DB2->where('group_id',$id);
            $this->DB2->update('permission_groups',$arr);
            
            $this->DB2->delete('group_permissions',array('group_id' => $id));
            
            if($id > 0)
            {
                for($x = 0 ; $x < sizeof($permissions) ; $x++)
                {
                    $permissions[$x]['group_id'] = $id;
                }
                $this->DB2->insert_batch('group_permissions',$permissions);
                return true;
            }
            return false;
        } catch (Exception $ex) {
            return false;
        } 
    }
    
    
    public function delete_permission_group($id)
    {
       try{
           $this->DB2->delete('permission_groups',array('group_id' => $id));
           return true;
       } catch (Exception $ex) {
           return false;
       }
    }
    
    
    public function get_permission_group($id)
    {
        $this->DB2->select('*');
        $this->DB2->from('permission_groups');
        $this->DB2->where('group_id',$id);
        $query = $this->DB2->get();
        return $query->row_array();
    }
    
    
    public function get_group_permissions($id)
    {
        /*$this->DB2->select('*');
        $this->DB2->from('group_permissions');
        $this->DB2->where('group_id',$id);
        $query = $this->DB2->get();*/
        $query = $this->DB2->query("SELECT a.*,b.menu_text FROM group_permissions a,menu_list b WHERE a.group_id=".$id." AND "
                . "a.menu_id=b.menu_id");
        return $query->result_array(); 
    }
    
    
    public function get_permissions_groups($start,$length,$search,$order_column)
    {
        $query = $this->DB2->query("SELECT * FROM permission_groups WHERE group_id LIKE '%".$search."%' OR group_name LIKE '%".$search."%' "
                . "ORDER BY ".$order_column." LIMIT ".$start.",".$length);
        return $query->result_array();
    }
    
    
    public function get_permissions_groups_count($search)
    {
        $query = $this->DB2->query("SELECT COUNT(group_id) AS row_count FROM permission_groups WHERE group_id LIKE '%".$search."%' OR group_name LIKE '%".$search."%'");
        $res = $query->row_array();
        return $res['row_count'];        
    }
    
}