<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Gate_Pass_Model extends CI_Model 
{ 
    private $DB1 = null;
    private $DB2 = null;
    
    public function __construct() 
    { 
        parent::__construct();  
        //$this->DB1 = $this->load->database('default',true);
        $this->DB2 = $this->load->database('second',true);
    } 
    
    public function get_max_gate_pass()
    {
        $this->DB2->select_max('id', 'gp_no');
        $query = $this->DB2->get('ngp_header'); 
        $result = $query->row_array();
        return $result['gp_no'];
    }
    
    
    public function insert_gate_pass()
    {
        try {
            $gp_header = $this->input->post('gp_header');
            $gp_details = $this->input->post('gp_details');    
        	
            $gp_header['user'] = $this->session->userdata('user_id');
            $gp_header['approve'] = 'pending';
                    
            $this->DB2->insert('ngp_header',$gp_header);
            $id = $this->DB2->insert_id();        
            
            if($id > 0)
            {
                for($x = 0 ; $x < sizeof($gp_details) ; $x++)
                {
                    $gp_details[$x]['gp_no'] = $id;
                }
                $this->DB2->insert_batch('ngp_details',$gp_details);
                return $id;
            }
            return false;
        } catch (Exception $ex) {
            return false;
        }        
    }
    
    
    public function update_gate_pass()
    {
        try {
            $gp_header = $this->input->post('gp_header');
            $gp_details = $this->input->post('gp_details');
            
            $gp_header['user'] = $this->session->userdata('user_id');

            $id = $this->input->post('id'); 
            $this->DB2->where('id',$id);
            $this->DB2->update('ngp_header',$gp_header);
            
            $this->DB2->delete('ngp_details',array('gp_no' => $id));
            
            if($id > 0)
            {
                for($x = 0 ; $x < sizeof($gp_details) ; $x++)
                {
                    $gp_details[$x]['gp_no'] = $id;
                }
                $this->DB2->insert_batch('ngp_details',$gp_details);
                return true;
            }
            return false;
        } catch (Exception $ex) {
            return false;
        } 
    }
    
    
    public function delete_gate_pass_items($gp_no)
    {
        $this->DB2->delete('ngp_details',array('gp_no' => $gp_no));
    }
    
    
    public function get_gate_passes($start,$length,$search,$order)
    {
        $str = "SELECT * FROM ngp_header WHERE id LIKE '%".$search."%' OR gp_no LIKE '%".$search."%' OR gp_to_address LIKE '%".$search."%' OR date LIKE '%".$search."%' OR style LIKE '%".$search."%' OR attention LIKE '%".$search."%' OR through LIKE '%".$search."%' OR remark LIKE '%".$search."%' OR instructed_by LIKE '%".$search."%' OR special_instruction LIKE '%".$search."%' ORDER BY ".$order." LIMIT ".$start.",".$length;
        $query = $this->DB2->query($str);
        return $query->result_array();
    }
    
    
    public function get_gate_passes_count($search)
    {
        $query = $this->DB2->query("SELECT COUNT(id) as row_count FROM ngp_header WHERE id LIKE '%".$search."%' OR gp_no LIKE '%".$search."%' OR gp_to_address LIKE '%".$search."%' "
                . "OR date LIKE '%".$search."%' OR style LIKE '%".$search."%' OR attention LIKE '%".$search."%' OR through LIKE '%".$search."%' "
                . "OR remark LIKE '%".$search."%' OR instructed_by LIKE '%".$search."%' OR special_instruction LIKE '%".$search."%'");
        $result = $query->row_array();
        return $result['row_count'];
    }
    
    
    public function get_gete_pass($id)
    {
        $this->DB2->select('*');
        $this->DB2->from('ngp_header');
        $this->DB2->where('id',$id);
        $query = $this->DB2->get();
        return $query->row_array();
    }
    
    
    public function get_gate_pass_items($id)
    {
        $this->DB2->select('line_no,description,unit,qty,status');
        $this->DB2->from('ngp_details');
        $this->DB2->where('gp_no',$id);
        $query = $this->DB2->get();
        return $query->result_array();
    }
    
    
    public function delete_gate_pass($id)
    {
        try{
            $this->DB2->delete('ngp_header',array('id' => $id));
            return true;
        } catch (Exception $ex) {
            return false;
        }       
    }
    
    
    public function change_gate_pass_status($id,$status)
    {
        try{
            if($status == 'approval pending')
                $this->DB2->query("UPDATE ngp_header SET approve='".$status."' WHERE id=".$id);
            else
                $this->DB2->query("UPDATE ngp_header SET approve='".$status."',approved_by='".$this->session->userdata('user_id')."',approved_date= WHERE id=".$id);
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }
    
    
}