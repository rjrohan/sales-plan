<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Sales_Plan_Model extends CI_Model 
{ 
    private $DB1 = null;
    private $DB2 = null;
    
    public function __construct() 
    { 
        parent::__construct();  
        $this->DB1 = $this->load->database('default',true);
        $this->DB2 = $this->load->database('second',true);
    }
    
    public function get_buyers()
    {
        $query = $this->DB1->query("SELECT IFSAPP.SALES_REGION.REGION_CODE,IFSAPP.SALES_REGION.DESCRIPTION FROM IFSAPP.SALES_REGION");
        return $query->result_array();
    }
    
    public function get_customer_order_qty($brand,$date)
    {
        //commented since 2015-12-09 
        //$query = $this->DB1->query("SELECT SUM(IFSAPP.CUSTOMER_ORDER_LINE.BUY_QTY_DUE * IFSAPP.CUSTOMER_ORDER_LINE.SALE_UNIT_PRICE) AS SUM_QTY FROM IFSAPP.CUSTOMER_ORDER INNER JOIN IFSAPP.CUSTOMER_ORDER_LINE ON IFSAPP.CUSTOMER_ORDER.ORDER_NO = IFSAPP.CUSTOMER_ORDER_LINE.ORDER_NO WHERE TO_CHAR(IFSAPP.CUSTOMER_ORDER.WANTED_DELIVERY_DATE, 'YYYY-MM') = '".$date."' AND IFSAPP.CUSTOMER_ORDER.REGION_CODE = '".$brand."'");
        $query = $this->DB1->query("SELECT SUM(IFSAPP.CUSTOMER_ORDER_LINE.BUY_QTY_DUE * IFSAPP.CUSTOMER_ORDER_LINE.SALE_UNIT_PRICE) AS SUM_QTY FROM IFSAPP.CUSTOMER_ORDER
INNER JOIN IFSAPP.CUSTOMER_ORDER_LINE ON IFSAPP.CUSTOMER_ORDER.ORDER_NO = IFSAPP.CUSTOMER_ORDER_LINE.ORDER_NO WHERE TO_CHAR(IFSAPP.CUSTOMER_ORDER.WANTED_DELIVERY_DATE, 'YYYY-MM') = '".$date."' 
AND IFSAPP.CUSTOMER_ORDER_LINE.STATE <> 'Cancelled' AND IFSAPP.CUSTOMER_ORDER.STATE <> 'Cancelled' AND IFSAPP.CUSTOMER_ORDER.REGION_CODE = '".$brand."'");
        $result = $query->row_array();
        return $result['SUM_QTY'];
    }
    
    public function insert_sales_plan($plan)
    {
        $data_arr = array();
        foreach($plan as $row)
        {
            $data = array(
                'buyer' => $row['buyer'],
                'sales_month' => $row['month'],
                //'type' =>  '',
                //'qty' => 0,
                'qty_target' => $row['target'],
                'qty_projection' => $row['projection'],
                'qty_previous' => $row['previous']
            );
            array_push($data_arr, $data);
            //$this->DB1->insert('sales_plan',$data);
            //$str = "INSERT INTO sales_plan (buyer,sales_month,qty_target,qty_projection,qty_previous) VALUES('".$row['buyer']."','".$row['month']."',".$row['target'].",".$row['projection'].",".$row['previous'].")";
            //$this->DB2->query($str);
        }
        $this->DB2->insert_batch('sales_plan',$data_arr);    
    }
    
    public function delete_sales_plan(/*$buyer,*/$month)
    {
        $this->DB2->delete('sales_plan',array(/*'buyer' => $buyer , */'sales_month' => $month));
    }
    
    public function get_sales_plan($buyer,$month)
    {
        $this->DB2->select('*');
        $this->DB2->from('sales_plan');
        $this->DB2->where('buyer',$buyer);
        $this->DB2->where('sales_month',$month);
        //$this->DB2->where('type',$type);
        $query = $this->DB2->get();
        return $query->row_array();
    }
    
}