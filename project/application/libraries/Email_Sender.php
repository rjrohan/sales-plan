<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Email_Sender {

    public function send_mail($arr = null)
    {
        $CI =& get_instance();
         $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_port' => 465,
            'smtp_user' => 'fdnstores@jinadasa.com', // change it to yours
            'smtp_pass' => 'Abcd@1234#', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
          );
        $CI->load->library('email', $config); 
        foreach($arr as $row)    
        {
            $message = $CI->load->view('email/email',$row['html_data'],true);            
            $CI->email->set_newline("\r\n");
            $CI->email->from('fdnstores@jinadasa.com','Foundation Garments (Pvt) Ltd'); // change it to yours
            $CI->email->to($row['to']);// change it to yours
            $CI->email->subject($row['subject']);
            $CI->email->message($message);
            if($row['attachments'] != null && $row['attachments'] != false)
            {
                foreach($row['attachments'] as $ath)
                {
                    $CI->email->attach($ath);
                }
            }           
            if($CI->email->send())
            {
                return true;//echo 'Email sent.';
            }
            else
            {
                return false;//show_error($CI->email->print_debugger());
            } 
        }
        
    }
}
